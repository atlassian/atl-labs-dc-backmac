# backmac

## What it does

Backmac is an [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) app for performing backups of Atlassian Data Center applications (Jira, Confluence, Crowd) running in AWS as CloudFormation stacks or as [Kubetnetes deployments on EKS](https://atlassian.github.io/data-center-helm-charts/).

Once deployed, a Cloudwatch rule will execute at the specified cron schedule to trigger the _backmac\_runbackups_ Lambda. This will look for all CloudFormation stacks or Kubernetes statefulsets with the tag/label "backmac_enabled" set to "true" and will add them to an SQS queue before triggering the execution of a State Machine. The State Machine (run once per service, sequentially) will orchestrate the snapshotting of volumes and DB instances in-use by Atlassian services, and the copying of those snapshots to a Disaster Recovery (DR) region.

Another Cloudwatch rule will also trigger the snapshot cleaner, which will delete any snapshots older than their tagged 'backup_delete_after' date (defaults to 30 days; configurable via a "backmac_retention_days" tag on each CloudFormation stack or Kubernetes statefulset targeted for backup).

**NOTE**:

Backmac is an unsupported backup solution designed by Atlassian.  Atlassian takes no responsibility for your use of Backmac. The apps are often developed in conjunction with Atlassian hack-a-thons and ShipIt days and are provided to you completely “as-is”, with all bugs and errors, and are not supported by Atlassian. Unless licensed otherwise by Atlassian, Backup Machine is subject to Section 14 of the Cloud Terms of Service – https://www.atlassian.com/legal/cloud-terms-of-service – or Section 8 of the Software License Agreement – https://www.atlassian.com/legal/software-license-agreement – as applicable, and are "No-Charge Products" as defined therein.


## Prerequisites

### Python

Backmac requires Python >= 3.12.

### AWS

To deploy Backmac, you must have the [AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html) installed, your environment must be configured with [AWS credentials](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-getting-started-set-up-credentials.html), and the access keys or IAM role you're using must have enough permissions to upload Lambdas and deploy the CloudFormation stack via the SAM CLI. Generally, you'll need administrator level of access; either the AWS managed "AdministratorAccess" policy or the effective "Allow *" for at least the following services:

* EC2
* EKS
* IAM
* Lambda
* CloudWatch (Rule and Logs)
* S3
* SQS
* SSM
* States
* VPC

## Deploying Backmac

### Template Parameters

A quick overview of the parameters for Backmac's SAM template. We provide sensible defaults where appropriate.

* **AccessCIDR**
  The CIDR IP range that is permitted to access Backmac's EC2 instance(s) for SSH access.

* **BackmacDRRegion**
  The region that snapshots of EFS and EBS volumes and RDS instances will be copied to.

* **BackmacInstanceSubnet**
  The subnet in your VPC that Backmac's EC2 instances will operate in. Currently, Backmac only supports being deployed in a single subnet/AZ.

* **BackupSchedule**
  A cron-like expression which controls how often Backmac will run (note: execution times are UTC).

* **CleanupSchedule**
  A cron-like expression which schedules the regular cleanup of expired snapshots (in UTC).

* **CustomIamRoleArn**
  Allows managing Backmac's IAM role outside of the Backmac stack itself; useful for compliance with company/org requirements around the management of IAM roles (see "Custom IAM Role" below).

* **EKSClusterName**
  Optional; provide cluster name if there are statefulsets in an EKS cluster labeled with "backmac_enabled=true" for backing up. Must be within the selected VPC. See "Access to EKS Kubernetes clusters via IAM and RBAC" below for instructions on creating a `ClusterRole` and a `ClusterRoleBinding` and associating them with an IAM role created by backmac.

* **KeyName**
  The EC2 Key Pair to allow SSH access to the instances.

* **LatestAmiSsmParam**
  SSM Parameter Store path for retrieving the latest AMI ID.

* **LocalAnsibleGitBranch**
  The branch to use for local-ansible git repo; optional, and will defer to default branch for repo if not provided

* **LocalAnsibleGitRepo**
  For additional node customization, provide repo location to git clone. This must be in format git@bitbucket.org:atlassian/atlassian-local-ansible-example.git if using Bitbucket Cloud, or ssh://git@stash.example.com:7997/my-local-ansible.git (ie protocol included) if using Bitbucket Server.

* **LocalAnsibleGitSshKeyName**
  If your git repo is private, provide an AWS Systems Manager ParameterStore key name that holds an SSH private key that can access the repo.

* **LocalAnsibleScriptPath**
  The path (no leading slash) and name of the script in your local-ansible repo which should be executed on nodes created by this stack.

* **LogGroupRetentionPeriod**
  The retention period for Cloudwatch log groups used by backmac's Lamdbas.

* **S3BucketName**
  S3 bucket name for storing `parallel_sync` script and output logs from SSM commands; if not specfied, one will be created.

* **S3Prefix**
  The prefix within the S3 bucket for storage of `parallel_sync` script and output logs from SSM commands; do not include leading/trailng slash; used regardless of value (or lack of one) for S3BucketName.

* **TerminateMinutes**
  Number of minutes after which backmac's EC2 node(s) will be terminated.

* **WebhookNotificationURL**
  A webhook for sending notifications of Backmac's progress for each run (successes, failures, etc). Currently, this field supports either a Slack webhook URL (see "Slack Notifications" below), or any generic webhook which expects a JSON payload with either "text" or "message" fields.

* **VPC**
  The VPC that Backmac will operate in.

### Deploying via SAM

As mentioned above, your environment (local or CI) must have AWS credentials and the SAM CLI installed. From there, it should be as simple as:

```
sam build
sam deploy --guided
```

The `--guided` flag will step through the deploy process, creating the SAM-managed deployment bucket, asking for values for the above parameters, permissions to create IAM roles, and whether to present the changeset for verification, and will save those values to the _samconfig.toml_ file enabling future deploys without the `--guided` flag.

Updates are handled via the same commands (`build` and `deploy`); you can either re-specify the `--guided` flag to update any template values in the interactive prompts, or you can just update the _samconfig.toml_ directly.

**Note:** We now use _samconfig.yaml_ for better templating of the config; at this time, the `--guided` flag does not have a method for writing to a YAML-based config file, so if you make use of the `--guided` flag, you'll need to manually update _samconfig.yaml_ with the values written to _samconfig.toml_.

### Access to EKS Kubernetes clusters via IAM and RBAC

**Note:** Backmac's support for Kubernetes-deployed DC applications currently assumes you are using the [Atlassian-provided Helm charts](https://atlassian.github.io/data-center-helm-charts/) and each namespace contains a single configmap with the name of `<namespace>-backmac` which contains two keys: `efs` and `rds` and those have the values of the efs id and rds id.

Backmac's lambdas need permission to query statefulset and custom resource information from your Kubernetes cluster. This is accomplished via a `ClusterRole` and `ClusterRoleBinding` linked to an IAM role via the "aws-auth" configmap. Backmac will create a standalone named IAM role for this purpose which grants no AWS permissions/policies except that it can be assumed by specific functions which require this access; the name of the IAM role will be in the format "$STACKNAME-$REGION-eks-cluster-api-access" and is listed in the SAM/stack outputs as `BackmacK8sClusterAccessRole`. If Backmac is configured with an `EKSClusterName` but the steps described here are not taken, Backmac will silently fail to find statefulsets, but will continue to back up Cloudformation-based services as-usual.

Sample configuration for creating the `ClusterRole` and `ClusterRoleBinding` is provided in _k8s-rbac.yaml_ in the root of this repository; apply it to your cluster like so:

```
kubectl apply -f k8s-rbac.yaml
```

Once the `ClusterRole` and `ClusterRoleBinding` have been created, edit the "aws-auth" configmap to associate the IAM role with the cluster user/role:

```
kubectl edit -n kube-system configmap/aws-auth
```

```
mapRoles: |
# ...
    - rolearn: <IAM role ARN; stack output "BackmacK8sClusterAccessRole">
      username: backmac
```

### Custom IAM Execution Role

If your org has requirements around IAM roles which would be easier to comply with if the IAM role is defined separately from Backmac's primary stack, a standalone IAM role which can be used by Backmac is provided via the _template-custom-iam-role.yaml_ Cloudformation template. Simply deploy the IAM role template – making any tweaks necessary to comply with your org's requirements – and supply the ARN of the IAM role to Backmac's `CustomIamRoleArn` template parameter.

### Local Ansible

The EC2 instance used by Backmac during each run of the State Machine is configured to allow the execution of an Ansible playbook, in the event that further configuration of the instance is required for compliance with company/organizational requirements. An example of this kind of "local ansible" script and playbook is provided here: https://bitbucket.org/atlassian/atlassian-local-ansible-example

### Slack Notifications

Backmac supports sending Slack-specific webhook payloads if a Slack webhook URL is provided. These messages will contain more information than the standard webhook payload (e.g. duration of state machine run, stack traces from failures, links to view CloudWatch logs, etc). No extra Backmac configuration is necessary to enable these features, but a slight amount of work is necessary on the Slack side of things:

1. You'll need a webhook URL, either by creating a Slack App or using a standard "Incoming WebHook" integration.
2. You'll need to configure the following emoji in your Slack instance (default icons provided in the _assets_ folder of this repo):
    * :aws_cloudwatch:
    * :aws_lambda:
    * :aws_stepfunctions:
    * :backmac_failure:
    * :backmac_info:
    * :backmac_success:
    * :backmac_warning:

### Warning

Backmac will not follow symlinks. If you need to backup things that are symlinked you may want to fork and modify the parallel_sync script.

[1]: https://bitbucket.org/atlassian/cloudtoken
[2]: https://aws.amazon.com/cli/

## Contributions

Backmac is not currently open to contributions and is provided "as-is".

## License

Copyright (c) 2018 - 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
