# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.14.1] - 2024-07-31
### Changed
- Updated dependencies


## [0.14.0] - 2024-07-08
### Added
- Shared layer function *ssm_wait_response* has a new kwarg (`exec_timeout`) to allow for faster failures of SSM commands to the EC2 instance

### Changed
- The _template-run-scheduled.yaml_ template now supports passing `instance_capacity_type` to *backmac_runbackups* via the `BackupInstanceCapacityType` template parameter
- Updated dependencies

### Fixed
- The *backmac_cleanup* Lambda could fail the entire execution if the `umount` command to unmount the EFS hung (even if/when the EFS might've been successfully unmounted); several changes aimed at making the cleanup step more resilient (use of the new `exec_timeout` kwarg, automatic retries on failure, etc.) were applied to prevent non-critical failures at this stage


## [0.13.0] - 2024-07-03
### Added
- Support for passing `instance_capacity_type` to *backmac_runbackups* (and via PUT requests to /backup); values of "spot" (the default) or "on-demand" allow specifying that a particular execution should skip straight to provisioning an "on-demand" instance instead of requesting a "spot" instance first

### Changed
- The *backmac_requeue_stack* Lambda now automatically sets `instance_capacity_type` to "on-demand" if the current execution failed due to a `Server.SpotInstanceTermination` error
- Updated dependencies

### Fixed
- The *backmac_ec2_start* Lambda could fail to provision an on-demand instance if/when no spot capacity was available


## [0.12.0] - 2024-07-01
### Changed
- EC2 instances created by Backmac are now tagged with service-specific tags for better attribution
- Instance requirements used to request EC2 instances now exclude the "inf2" instance type
- Python 3.11 → 3.12
- black → ruff
- samconfig.toml → samconfig.yaml
- Updated dependencies


## [0.11.1] - 2024-02-08
### Changed
- Removed leftover return values from debugging dynamodb:updateItem state machine task


## [0.11.0] - 2024-02-08
### Added
- API requests to GET a list of services via the /backups endpoint can now be filtered by service_name, status, and max_age of the available backups
- New indexes on the DynamoDB table to support the above filtering; as such, this release may require some manual effort to apply the required changes incrementally (ref: https://github.com/aws-cloudformation/cloudformation-coverage-roadmap/issues/229 and https://github.com/aws/aws-cdk/issues/12246)

### Changed
- Updated dependencies
- Dropped package management via Pipenv (was only being used by local development environments)


## [0.10.0] - 2023-11-09
### Added
- New template parameter `BackmacNodeVolumeType` allows specifying the type of EBS backup volume (gp2 or gp3); default is gp2 for backwards compatibilty

### Changed
- Python 3.10 → 3.11
- Updated dependencies

### Fixed
- Backups could fail due to flawed logic in *backmac_mount_ebs* which could fail to properly flag the volume for resize


## [0.9.6] - 2023-10-06
### Changed
- Updated dependencies


## [0.9.5] - 2023-08-21
### Changed
- Improved Kubernetes Support
- Updated dependencies


## [0.9.4] - 2023-06-29
### Changed
- EC2 instances created by Backmac now use Amazon Linux 2023 minimal AMI
- Updated dependencies


## [0.9.3] - 2023-05-18
### Changed
- Updated dependencies

### Fixed
- Backup runs could fail in some cases where files were modified between checksum calculation and transfer by rsync; the `rsync_ignore_vanished` wrapper in *parallel_sync* has been tuned to ignore these errors


## [0.9.2] - 2023-05-16
### Added
- Added an "8x" option for `instance_size`

### Changed
- Adjusted the memory and vCPU parameters for sizing and other requirements to select from a wider pool of EC2 instance classes
- Updated dependencies

### Fixed
- The `instance_size` parameter is now also passed through to retries for backup runs where the parameter was present

## [0.9.1] - 2023-05-11
### Added
- Support for backup volumes formatted with an XFS file system

### Changed
- *backmac_mount_ebs* now creates XFS filesystems by default for new volumes


## [0.9.0] - 2023-05-11
### Added
- Support for passing `instance_size` to *backmac_runbackups* (with value of "1x", "2x" or "4x"; default is "2x") to choose corresponding sizes of EC2 instances using the already-defined instance requirements; i.e., today this selects either xlarge, 2xlarge, or 4xlarge sizes of the c5, c5a, c6a, c6g, c6gn, c6i, c6in, and c7g instance types
- Support for scheduling backups for an individual stack to run on a different schedule from the "all stacks" schedule via the new _template-run-scheduled.yaml_ template

### Changed
- *backmac_ec2_start* now uses `price-capacity-optimized` instead of `capacity-optimized` when reqeusting spot instances
- Updated dependencies


## [0.8.0] - 2023-05-09
### Added
- Support for Jira 9

### Changed
- Python 3.9 → 3.10
- Updated dependencies

### Fixed
- Directories in the top layer handled by *parallel_sync* should now always be owned by the product owner instead of `root:root`


## [0.7.4] - 2023-04-03
### Changed
- API requests to PUT a service to /backup now return 409 response for duplicate backup requests
- Various IAM policies updated to include `cloudformation:ListStacks` permission where appropriate (DescribeStacks without a target stack)
- Updated dependencies

### Fixed
- Better error handling and DLQ delivery for services where the persistent storage or database for a service can't be discovered in *backmac_queue_handler*


## [0.7.3] - 2023-01-09
### Fixed
- Revised support for Bitbucket in *parallel_sync* to more-accurately target the structure of the shared-home for backups


## [0.7.2] - 2023-01-05
### Changed
- Updated dependencies

### Fixed
- Some products (Bitbucket and Crowd) do not have index snapshots to be measured during volume resize
- Fixed an issue where *parallel_sync* could fail on newly-created volumes due to non-existent target directories (a result of rsync running in parallel across randomized target directories)
- For Confluence, check for attachments/ver003/nonspaced directory before attempting to traverse its sub-directories

### Security
- Pinned setuptools to >=65.5.1 to address CVE-2022-40897


## [0.7.1] - 2022-11-09
### Changed
- Updated dependencies

### Fixed
- Better handling of exceptions and timeouts related to snapshot waiters
- Don't store BackmacVolumeNotFound exception data on the event (can mask further failures)
- Empty event values for `webhook_notification_url` could cause queueing failures


## [0.7.0] - 2022-11-08
### Added
- Backmac now supports backups of Bitbucket Data Center, including support for taking snapshots of Bitbucket Mesh storage volumes
- The retention period for CloudWatch log groups managed by this stack is now set via a new CloudFormation custom resource, *cfn_set_lambda_log_group_retention*
- To support parallel execution and provide better management of execution runs and status, Backmac now stores a limited amount of SQS and State Machine data in a DynamoDB table

### Changed
- Backup runs are now executed in parallel, and the State Machine and SQS queue have been slightly refactored to support this change:
    - *backmac_runbackups* has been trimmed down to support direct invocation for a given stack (either via CLI or API)
    - New function *backmac_queue_backups* inherits the parts of *backmac_runbackups* that used to do service discovery for regular/scheduled backup runs
    - The SQS queue is now a FIFO queue and has a corresponding DLQ for handling queuing and backup failures
    - *backmac_read_queue* has been renamed *backmac_queue_handler* and has been refactored to be invoked directly by the SQS queue
    - Execution names in the State Machine now include the name of the service being backed up for each run
    - All State Machine execution failures now automatically trigger a retry run; a second failed run will be re-queued to the DLQ
- Tagging of backup volumes and snapshots has been refactored; each object being tagged (backup volume, snapshot) uses data from every available level above that object (i.e., service -> state machine context -> backup instance -> source volume -> backup volume -> snapshot)
- Management of KMS key grants and DR key creation (if necessary) has been refactored; the default behavior now is to create (if it doesn't already exist) a KMS key of the same name/alias in the DR region and to always use that key if the source volume was encrypted (previously, AWS would use the default AWS-managed KMS key if the source volume was encrypted but no KMS key was provided when copying a snapshot)
- A resized EBS backup volume no longer triggers a re-queue of the service; after resize, the State Machine throws back to the `ebsvolume_waiter` step to wait for the initial volume modification to complete, and then the backup run continues as-normal while volume optimization occurs in the background
- Updated dependencies

### Removed
- The `store_lambda_env` function has been removed from *backmac_utils.py*; the original purpose of this function was to provide error context to the *backmac_fail* catch-all error handler; in reality, this has never worked properly because event handlers don't return the “event” object from a failed lambda, so the data we end up storing is always for the Lambda before the failure, which isn't very helpful
- The CloudWatch log group definitions for Lambda functions managed by this stack have been removed in favor of letting Lambda manage the log groups (which were only previously defined so retention could be managed; this is now handled by *cfn_set_lambda_log_group_retention*)
- We no-longer create KMS keys in the DR region on the pattern `$servicename-backmac-dr`; this key isn't actually being used anywhere

### Fixed
- Backup runs for a specific EBS volume ID and RDS ID could still spin up an unnecessary EC2 instance; this loop in the state machine is now skipped entirely if no user-defined or service-discovered EFS ID is present in the backup request
- Snapshots of backup volumes were previously taken before the volume was unmounted; this could lead to journal corruption, so the order of operation there has been flipped
- Backup runs that failed could impact further execution of the State Machine in such a way that no further queued services would be read from the queue; this behavior isn't possible now as backup runs are no longer responsible for queuing the next run


## [0.6.0] - 2022-09-13
### Added
- Backmac API v1 – powered by API Gateway – with routes for adding a backup to the queue, listing backups in the queue and the execution IDs of backups in-progress, and getting the execution details for a particular/given execution ID, including the list of snapshots produced by that backup run
- New Lambda functions *backmac_api_list_backups* and *backmac_api_get_backup* provide the backend logic for the "list" and "get" routes as described above
- New template parameter `CreateApi`; boolean which controls whether API resources are created or not
- New template parameter `HostedZoneDomain`; provides the base domain for the API; required if `CreateApi` is "true"
- New template parameter `SSLCertificateARN`; provides SSL encryption for the API; required if `CreateApi` is "true"

### Changed
- Updated dependencies


## [0.5.5] - 2022-07-25
### Changed
- Updated dependencies


## [0.5.4] - 2022-07-13
### Changed
- Updated dependencies


## [0.5.3] - 2022-07-13
### Changed
- All dependencies were pinned to their existing versions


## [0.5.2] - 2022-07-07
### Changed
- *backmac_ebs_status* function updated to allow backups to proceed once EBS backup volume is in "optimizing" state after a resize


## [0.5.1] - 2022-06-23
### Changed
- The `ExecutionTimeout` for the *parallel_sync* SSM command has been reduced from 6 to 3 hours

### Fixed
- The State Machine now loops back through the *backmac_ec2_status* function during the *efs2ebs_status* status loop to catch EC2 instance failures/terminations during *parallel_sync*
- Shared layer function *ssm_wait_response* has been updated to handle (and raise) EC2 instance terminations that occur during SSM command execution
- Addressed a cascade failure that could occur when the EC2 instance was terminated during the run, where during cleanup the SSM commands to unmount the backup volume would fail
- Backups could fail during *backmac_mount_ebs* if/when `e2fsck` modified the filesystem (better handling of `e2fsck` return codes)


## [0.5.0] - 2022-06-14
### Changed
- The *backmac_ec2_start* Lambda function now uses an EC2 Fleet request to create the EC2 instance for a given run; this allows requesting an instance from many classes based on size and architecture parameters instead of a single instance class, which could lead to missed backup runs when capacity for the chosen instance class wasn't available

### Removed
- The `NodeInstanceType` parameter has been removed as instance selection is now handled by the EC2 Fleet request and is no longer tied to a single instance class


## [0.4.6] - 2022-06-09

- Internal release; no external changes


## [0.4.5] - 2022-04-25
### Changed
- The *parallel_sync* script now downloads a pinned/released version of the [GNU `parallel` utility](https://www.gnu.org/software/parallel/) instead of relying on the latest version from the master branch in [parallel's git repo](https://git.savannah.gnu.org/cgit/parallel.git/)


## [0.4.4] - 2022-04-08
### Added
- The *parallel_sync* script now includes a 5th layer of recursive sync on nonspaced attachments to improve runtimes for Confluence backups

### Changed
- The *parallel_sync* script now passes `--delete-during` on rsync layers 4 and 5 to clean up old files (mostly attachments) at these lower levels in the file hierarchy


## [0.4.3] - 2022-04-01
### Added
- The *parallel_sync* script now saves a job log for each parallel layer of `rsync` jobs; these are saved to _/var/log_ in the format "parallel_sync_layerX.log", where X matches a given parallel layer

### Changed
- To improve efficiency, the *parallel_sync* script now uses `nproc` to determine the number of parallel `rsync` jobs to run, instead of relying on `parallel` to determine this value natively


## [0.4.2] - 2022-02-02
### Added
- New parameter `LocalAnsibleGitBranch` allows specifying a branch to use with repo provided via existing parameter `LocalAnsibleGitRepo`

### Changed
- To save a little time, any repo specified via the `LocalAnsibleGitRepo` parameter will be cloned to a depth of "1"


## [0.4.1] - 2022-02-01
### Changed
- The State Machine now uses the "/aws/vendedlogs/" prefix for logs from the State Machine itself; see [Amazon CloudWatch Logs resource policy size restrictions](https://docs.aws.amazon.com/step-functions/latest/dg/bp-cwl.html) for more information
- The *parallel_sync* script no longer synchronizes Jira's index snapshots stored in the */caches* directory of the shared_home; these snapshots are only used for Jira's internal function for sharing index snapshots between nodes and are not used for index restoration via any other method (and thus can be safely excluded from backups)

### Fixed
- A bug in the *parallel_sync* script could leave certain folders owned by root instead of the application user


## [0.4.0] - 2021-10-26
### Added
- Parameter `EKSClusterName` allows setting the name of an EKS cluster that Backmac should search for Atlassian DC applications running as Kubernetes statefulsets to backup (in addition to any Cloudformation stacks). This functionality assumes you're using the [Atlassian-provided Helm charts](https://atlassian.github.io/data-center-helm-charts/) and [Crossplane](https://crossplane.io/) as an add-on to provide CRDs for EFS filesystems and RDS databases; see README.md for more information
- New Cloudformation custom resource `GetEKSClusterNetworkingCustomResource` finds EC2 Security Groups and Subnets associated with the cluster name provided via `EKSClusterName` and configures *backmac_runbackups* and *backmac_read_queue* Lambda functions with `VpcConfig` params
- Backmac stack output `BackmacK8sClusterAccessRole` provides the ARN of the IAM role for use with access/ingress to a Kubernetes cluster, as descirbed in README.md under "Access to EKS Kubernetes clusters via IAM and RBAC"
- The *backmac_runbackups* Lambda function now accepts arbitrary EFS and RDS IDs (via `efs_id` and `rds_id` parameters on the invocation event object) to backup Atlassian DC applications in AWS that might not be deployed via Cloudformation or an EKS Kubernetes cluster, but which still use RDS and EFS for the database and persistent storage, respectively

### Changed
- Lambda functions *backmac_runbackups* and *backmac_read_queue*, as well as the Backmac Step Functions state machine now define their own Lambda permissions policies, in pursuit of a least-privilege model of permissions for backmac
- Copied EBS snapshots are now tagged at the point of copy, instead of after copy
- Various minor improvments including standardization of module imports, line length, and type hinting across all functions

### Removed
- Support for backing up "server" instances of Atlassian applications (i.e., non-DC, non-clustered)

### Fixed
- When the run is complete we now raise custom exception `BackmacEmptyQueue` instead of relying on `KeyError` which can be thrown by other code and result in a false positive of a completed run


## [0.3.0] - 2021-08-12
### Added
- Parameter `LogGroupRetentionPeriod` allows setting the number of days to retain CloudWatch log groups for Backmac's AWS Lambda functions
- Parameter `CleanupSchedule` allows configuring a separate EventBridge schedule for cleanup of EBS/RDS snapshots past their expiry date to avoid AWS API throttling issues (clashing with regular Backmac executions)
- Stack export `BackmacS3BucketAndPrefix` allows referencing which bucket and prefix is being used by Backmac
- New standalone SAM template for cleaning snapshots from the DR region

### Changed
- Function *backmac_snapshot_cleaner* now defines its own IAM policy, is granted more memory (128 -> 192), and has the maximum execution timeout allowed (15m)


## [0.2.0] - 2021-07-23
### Added
- Backmac is now packaged/deployed via the [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) framework and [CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-command-reference.html)
- Stack-managed S3 bucket (or custom bucket and prefix supplied by `S3BucketName` and `S3Prefix` stack params) stores SAM deployment resources, SSM command logs (stdout/err), and *parallel_sync* script
- *parallel_sync* script is deployed to S3 via Lambda-backed Cloudformation custom resource `cfn_push_parallel_sync_to_s3`
- Started added type hints to Lambdas that were modified for other reasons
- Added new "BackmacBoto3" layer attached to all functions; packages latest boto3 and botocore instead of relying on version available in runtime environment
- This changelog

### Changed
- Use `ssm_wait_response` from "BackmacCommon" layer (*backmac_utils.py*) everywhere we currently use disparate versions of `ssm_wait_response`
- Send SSM execution logs to stack-managed/configured bucket (i.e., not hard-coded to "wpe-logs" bucket)
- Removed legacy testing blocks from the tail end of each Lambda
- Moved `get_backup_volume` function from *backmac_ebs_status* and *backmac_mount_ebs* Lambdas to BackmacCommon layer (*backmac_utils.py*)

### Removed
- Publicly-available Lambda functions and other resources (parallel_sync) are no-longer published to S3
- *package.sh* packaging script

### Fixed
- `e2fsck` could fail where it wasn't being called with `-y` flag to force non-interactive mode
- *backmac_ebs_status* and *backmac_mount_ebs* Lambdas could throw `IndexError` for reasons other than a missing volume (which we were using in the state machine logic to trigger creation of a new backup volume), which led to the occasional duplicate backup volume being created for a stack; we now throw custom `BackmacVolumeNotFound` exception instead
- Better handling of different stack trace formats in error notifications to Slack
- Resizing EBS volumes could fail with a nebulous "couldn't convert to float" error if index snapshots couldn't be found (i.e., if index snapshots are not enabled); we now catch and raise a specific error message for this


## [0.1.0] - 2018 to 2021-07-14
### Added
- All development on backmac prior to this date is considered to be the "first version"
