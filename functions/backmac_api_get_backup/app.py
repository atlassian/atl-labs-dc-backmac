#!/usr/bin/env python3

from __future__ import annotations

import json
import logging
import os

from typing import Any

import boto3

from backmac_utils import BOTO_CONFIG, to_utc_iso_str

DYNAMODB = boto3.resource("dynamodb", config=BOTO_CONFIG)
SFN = boto3.client("stepfunctions", config=BOTO_CONFIG)

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_PARTITION = os.environ["AWS_PARTITION"]
AWS_REGION = os.environ["AWS_REGION"]

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_backup_details(sfn_name: str, execution_name: str):
    execution_arn = f"arn:{AWS_PARTITION}:states:{AWS_REGION}:{AWS_ACCOUNT_ID}:execution:{sfn_name}:{execution_name}"
    execution_details = SFN.describe_execution(executionArn=execution_arn)
    execution_history = SFN.get_execution_history(executionArn=execution_arn, reverseOrder=True, maxResults=20)

    last_known_task_details = next(
        (step for step in execution_history["events"] if step["type"] in {"TaskStateExited", "TaskStateEntered"}),
        {"type": "Unknown"},
    )
    event_details_key = next(key for key in last_known_task_details.keys() if key.endswith("EventDetails"))
    task_name = last_known_task_details[event_details_key]["name"]  # type: ignore
    # the state machine itself will only have "output" if the task succeeds; otherwise we'll need event data from the last step that had it
    input_or_output = json.loads(
        next((v for k, v in last_known_task_details[event_details_key].items() if k in {"input", "output"}), "{}")
    )  # type: ignore

    backup_details: dict[str, Any] = {
        "id": json.loads(execution_details["input"])["sqs_details"]["message_id"],
        "name": execution_name,
        "execution_details": {
            "status": execution_details["status"],
            "last_state": task_name,
            "start_tstamp": to_utc_iso_str(execution_details["startDate"]),
        },
    }

    if "dr_region" in input_or_output:
        backup_details["dr_region"] = input_or_output["dr_region"]

    if "service" in input_or_output:
        backup_details["service"] = {k: v for k, v in input_or_output["service"].items() if k != "labels"}

    if execution_details["status"] != "RUNNING":
        backup_details["execution_details"]["end_tstamp"] = to_utc_iso_str(execution_details["stopDate"])

    if execution_details["status"] == "FAILED":
        backup_details["execution_details"]["error_msg"] = (
            f"{input_or_output['error']['Error']}: {input_or_output['error_details']['cause']['errorMessage']}"
        )

    if execution_details["status"] == "SUCCEEDED":
        # since we know the state machine finished successfully, use the output from the state machine from this point
        execution_output = json.loads(execution_details["output"])
        if "snapshots" in execution_output:
            backup_details["snapshots"] = execution_output["snapshots"]
        if "dr_snapshots" in execution_output:
            backup_details["dr_snapshots"] = execution_output["dr_snapshots"]

    return backup_details


def lambda_handler(request, context):
    logger.info(f"request: {request}")

    # normally we'd set the required params at routing level in the API schema, but neither Swagger 2.0 nor API
    # Gateway itself currently support the "oneOf" directive, so we have to do it ourselves for now
    # make sure there are parameters; queryStringParameters will be None if no parameters were provided
    if not request["queryStringParameters"]:
        return {
            "statusCode": 400,
            "body": json.dumps({"error_msg": "Missing required request parameters: oneOf[backup_id, backup_name]"}),
        }

    # make sure at least one of the required params was provided
    one_of_required_params = [key in request["queryStringParameters"].keys() for key in ["backup_id", "backup_name"]]
    if not any(one_of_required_params):
        return {
            "statusCode": 400,
            "body": json.dumps({"error_msg": "Missing required request parameters: oneOf[backup_id, backup_name]"}),
        }

    # make sure we don't have both of the params we're looking for
    if all(one_of_required_params):
        return {
            "statusCode": 400,
            "body": json.dumps({"error_msg": "Too many request parameters; expected: oneOf[backup_id, backup_name]"}),
        }

    # at this point we should have either backup_id or backup_name; if we have backup_name just use it, otherwise use
    # backup_id to look up the backup_name from DynamoDB
    if "backup_name" in request["queryStringParameters"]:
        execution_name = request["queryStringParameters"]["backup_name"]
    else:
        backup_history = DYNAMODB.Table(os.environ["backup_history_table_name"])
        history_item = backup_history.get_item(
            Key={"sqs_message_id": request["queryStringParameters"]["backup_id"]}, ConsistentRead=True
        )
        try:
            execution_name = history_item["Item"].get("execution_name")
        except KeyError:
            return {
                "statusCode": 400,
                "body": json.dumps({"error_msg": "Invalid/unknown backup_id"}),
            }

    try:
        backup_details = get_backup_details(os.getenv("sfn_name"), execution_name)
    except (SFN.exceptions.InvalidArn, SFN.exceptions.ExecutionDoesNotExist) as e:
        return {
            "statusCode": 400,
            "body": json.dumps({"error_msg": f"Invalid/unknown backup_name: {e.response['Error']['Message']}"}),
        }
    except Exception as e:
        logger.exception(e)
        return {
            "statusCode": 500,
            "body": json.dumps({"error_msg": f"Unhandled exception: {getattr(e, 'message', repr(e))}"}),
        }
    else:
        return {"statusCode": 200, "body": json.dumps(backup_details)}
