#!/usr/bin/env python3

from __future__ import annotations

import os

import boto3

from backmac_exceptions import SsmExecutionError
from backmac_utils import BOTO_CONFIG, ssm_wait_response
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance

EC2 = boto3.resource("ec2", config=BOTO_CONFIG)
SSM = boto3.client("ssm", config=BOTO_CONFIG)


def ssm_send_command(backmac_instance: Instance, cmd: str) -> str:
    command_params: dict = {
        "InstanceIds": [backmac_instance.id],
        "DocumentName": "AWS-RunShellScript",
        "Parameters": {"commands": [cmd], "executionTimeout": ["10800"]},
    }

    try:
        command_params["OutputS3BucketName"] = os.environ["backmac_s3_bucket"]
        command_params["OutputS3KeyPrefix"] = "/".join([os.environ["backmac_s3_prefix"], "ssm_commands"])
    except KeyError:
        pass

    ssm_command = SSM.send_command(**command_params)

    print(f'for command: {cmd} CommandId is: {ssm_command["Command"]["CommandId"]}')
    return ssm_command["Command"]["CommandId"]


def lambda_handler(event, context):
    try:
        backmac_instance = EC2.Instance(event["backmac_instance"])
    except Exception:
        raise Exception("No backmac_instance found in event")

    # validate there is some backup storage mounted
    try:
        ssm_wait_response(backmac_instance, "df -h /backup && df -h /media/atl")
    except SsmExecutionError as e:
        raise SsmExecutionError(
            "SSM command to check volumes prior to backup failed; parallel_sync will not be run!"
        ) from e

    rsync_command_id = ssm_send_command(backmac_instance, "/usr/bin/time /usr/local/bin/parallel_sync")

    event["rsync_command_id"] = rsync_command_id
    return event
