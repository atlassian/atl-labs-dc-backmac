#!/usr/bin/env python3

from __future__ import annotations

from typing import Callable, Literal, TYPE_CHECKING

import boto3
import botocore

from backmac_utils import BOTO_CONFIG

if TYPE_CHECKING:
    from backmac_types import ServiceSnapshotTypeDef, RdsSnapshotStateType
    from mypy_boto3_ec2.literals import SnapshotStateType as EbsSnapshotStateType


def ensure_ebs_snap_complete(ebs_snap: str, region: str) -> None:
    print(f"Ensuring snapshot complete for: {ebs_snap}")
    ec2 = boto3.client("ec2", region_name=region, config=BOTO_CONFIG)
    waiter = ec2.get_waiter("snapshot_completed")
    try:
        waiter.wait(SnapshotIds=[ebs_snap])
    except Exception as e:
        # we don't need to handle this exception; we'll check the status next and return 'waiting' which will invoke the
        # state machine's waiter step loop for this action
        print(f"Snapshot waiter failed: {e}")


def ensure_rds_snap_complete(rds_snap: str, region: str) -> None:
    print(f"Ensuring snapshot complete for: {rds_snap}")
    rds = boto3.client("rds", region_name=region, config=BOTO_CONFIG)
    waiter = rds.get_waiter("db_snapshot_completed")
    try:
        waiter.wait(DBSnapshotIdentifier=rds_snap)
    except Exception as e:
        # we don't need to handle this exception; we'll check the status next and return 'waiting' which will invoke the
        # state machine's waiter step loop for this action
        print(f"Snapshot waiter failed: {e}")


def check_ebs_snapshot_state(snapshot_id: str, region: str) -> tuple[EbsSnapshotStateType | Literal["waiting"], str]:
    print(f"Checking EBS snapshot state in {region} for {snapshot_id}")
    # use a region-specific client since we might be checking the snapshot status in the dr_region
    ec2 = boto3.client("ec2", region_name=region, config=BOTO_CONFIG)
    try:
        ebs_snapshot_status = ec2.describe_snapshots(SnapshotIds=[snapshot_id])
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response["Error"]["Code"] == "Throttling":
            print("describe_snapshots was throttled; returning to waiter")
            return "waiting", "unknown"
        raise
    ebs_snapshot_state = ebs_snapshot_status["Snapshots"][0]["State"]
    ebs_snapshot_progress = ebs_snapshot_status["Snapshots"][0]["Progress"]
    print(f"Snapshot status for {snapshot_id}: {ebs_snapshot_state}, {ebs_snapshot_state}")
    return ebs_snapshot_state, ebs_snapshot_progress


def check_rds_snapshot_state(snapshot_id: str, region: str) -> tuple[RdsSnapshotStateType | Literal["waiting"], str]:
    print(f"Checking RDS snapshot state in {region} for {snapshot_id}")
    # use a region-specific client since we might be checking the snapshot status in the dr_region
    rds = boto3.client("rds", region_name=region, config=BOTO_CONFIG)
    try:
        db_snapshot_status = rds.describe_db_snapshots(DBSnapshotIdentifier=snapshot_id)
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response["Error"]["Code"] == "Throttling":
            print("describe_db_snapshots was throttled; returning to waiter")
            return "waiting", "unknown"
        raise
    rds_snapshot_state = db_snapshot_status["DBSnapshots"][0]["Status"]
    rds_snapshot_progress = f"{db_snapshot_status['DBSnapshots'][0]['PercentProgress']}%"
    print(f"Snapshot status for {snapshot_id}: {rds_snapshot_state}, {rds_snapshot_progress}")
    return rds_snapshot_state, rds_snapshot_progress


def lambda_handler(snapshot: ServiceSnapshotTypeDef, context):
    state_checker_func_name: dict[str, Callable] = {
        "ebs": check_ebs_snapshot_state,
        "rds": check_rds_snapshot_state,
    }

    waiter_func_name: dict[str, Callable] = {
        "ebs": ensure_ebs_snap_complete,
        "rds": ensure_rds_snap_complete,
    }

    # check the status; maybe it's already done
    snap_status, progress = state_checker_func_name[snapshot["type"]](snapshot["id"], snapshot["region"])

    # return now if it's done, otherwise throw into the waiter
    if snap_status in {"available", "completed"}:
        return snapshot | {"status": snap_status, "progress": progress}
    else:
        waiter_func_name[snapshot["type"]](snapshot["id"], snapshot["region"])

    # having exited the waiter, check the status again and return
    snap_status, progress = state_checker_func_name[snapshot["type"]](snapshot["id"], snapshot["region"])
    return snapshot | {"status": snap_status, "progress": progress}
