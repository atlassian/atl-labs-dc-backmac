#!/usr/bin/env python3

from __future__ import annotations

import boto3

from backmac_utils import BOTO_CONFIG

rds = boto3.client("rds", config=BOTO_CONFIG)


def lambda_handler(event, context):
    rds_name: str = event["service"]["rds_id"]

    # Check for RDS Instance and its availability status.
    try:
        rds_status = rds.describe_db_instances(DBInstanceIdentifier=rds_name)
        rds_availability = rds_status["DBInstances"][0]["DBInstanceStatus"]
    except rds.exceptions.DBInstanceNotFoundFault as e:
        print(f'Unable to locate RDS DB instance "{rds_name}.')
        print(e)
        raise
    except Exception as e:
        print(f'Unable to fetch availability status for RDS DB instance "{rds_name}.')
        print(e)
        raise

    # Per https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Status.html
    # Aside from stopped/stopping, include other RDS status that should stop operation.
    rds_bad_status = [
        "stopped",
        "stopping",
        "deleting",
        "failed",
        "inaccessible-encryption-credentials",
    ]

    if rds_availability == "available":
        event["rds_status"] = "Ready"
    elif rds_availability in rds_bad_status:
        raise Exception(f'RDS "{rds_name}" stopped or not available')
    else:
        event["rds_status"] = "Pending"

    print(f"RDS status is: {event['rds_status']}")
    return event
