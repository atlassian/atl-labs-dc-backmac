#!/usr/bin/env python3

from typing import TYPE_CHECKING

import backmac_notify as notify

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef


def lambda_handler(params, context):
    event = params["event"]
    sfn_context = params["sfn_context"]

    message_parts: list[str] = ["Iteration successful"]
    service: BackmacServiceTypeDef = event.get("service")
    if service:
        if "name" in service and "iac" in service:
            message_parts.append(f"for {service['name']} ({service['iac']})")
        elif any(key for key in service.keys() if key.endswith("_id")):
            message_parts.append("for")
            message_parts.append(", ".join([f"{k}: {v}" for k, v in service.items() if k.endswith("_id")]))

    notify.send("success", " ".join(message_parts), event, sfn_context)
    return event
