#!/usr/bin/env python3

from __future__ import annotations

from os import getenv
from typing import Any, Optional, TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, clean_labels, labels_to_tags, tags_to_labels

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance

ec2 = boto3.resource("ec2", config=BOTO_CONFIG)
efs = boto3.client("efs", config=BOTO_CONFIG)


def create_backup_volume(
    backmac_instance: Instance,
    service_name: str,
    efs_size: int,
    service_labels: dict[str, str],
    efs_labels: dict[str, str],
    kms_key_arn: Optional[str],
):
    zone = backmac_instance.placement["AvailabilityZone"]
    volumesize = float(efs_size) / 1073742  # current disk space used in GiB(ish)
    # let's not go crazy with adding our 20% disk space buffer
    if (volumesize * 0.2) > 200:  # GiB
        volumesize = volumesize + 200
    else:
        volumesize = volumesize * 1.2
    # gp2/gp3 has constraints of 1-16384, so this might still fail if volumesize * 1.2 < 0.5GiB , so adding +1
    volumeparams: dict[str, Any] = {
        "AvailabilityZone": zone,
        "Size": int(round(volumesize + 1)),
        "VolumeType": getenv("backmac_node_volume_type", "gp2"),
    }

    volume_labels: dict[str, str] = {}
    volume_labels = volume_labels | clean_labels(service_labels)
    volume_labels = volume_labels | clean_labels(efs_labels)
    volume_labels = volume_labels | {
        "Name": f"{service_name}-backup",
        "created_by": "backmac",
    }
    volumeparams["TagSpecifications"] = [
        {
            "ResourceType": "volume",
            "Tags": labels_to_tags(volume_labels),
        }
    ]

    if kms_key_arn and len(kms_key_arn) > 0:
        volumeparams["Encrypted"] = True
        volumeparams["KmsKeyId"] = kms_key_arn
    response = ec2.meta.client.create_volume(**volumeparams)
    return response


def lambda_handler(event, context):
    backmac_instance = ec2.Instance(event["backmac_instance"])
    efs_kms_key_arn: Optional[str] = next(
        (key for key, services in event["source_kms_key_arns"].items() if "efs" in services), None
    )

    efs_labels = tags_to_labels(efs.list_tags_for_resource(ResourceId=event["service"]["efs_id"])["Tags"])

    response = create_backup_volume(
        backmac_instance,
        event["service"]["name"],
        event["efs_used_space"],
        event["service"]["labels"],
        efs_labels,
        efs_kms_key_arn,
    )
    print(response)
    return event
