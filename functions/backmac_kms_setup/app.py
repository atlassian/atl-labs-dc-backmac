#!/usr/bin/env python3

from __future__ import annotations

import os
from typing import Literal, TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, clean_labels

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef
    from mypy_boto3_kms.type_defs import AliasListEntryTypeDef, CreateGrantResponseTypeDef, TagTypeDef as KmsTagTypeDef

cfn = boto3.client("cloudformation", config=BOTO_CONFIG)
ec2 = boto3.client("ec2", config=BOTO_CONFIG)
efs = boto3.client("efs", config=BOTO_CONFIG)
rds = boto3.client("rds", config=BOTO_CONFIG)

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_PARTITION = os.environ["AWS_PARTITION"]
AWS_REGION = os.environ["AWS_REGION"]


def get_backmac_role() -> str:
    exports_dict = cfn.list_exports()
    backmac_dict = [resource for resource in exports_dict["Exports"] if resource["Name"] == "BackmacRole"]
    return backmac_dict[0]["Value"]


def create_kms_key(region: str, service: BackmacServiceTypeDef) -> str:
    kms_labels: dict[str, str] = {}
    kms_labels = kms_labels | clean_labels(service.get("labels", {}))
    kms_labels = kms_labels | {"created_by": "backmac"}
    kms_tags: list[KmsTagTypeDef] = [{"TagKey": key, "TagValue": value} for key, value in kms_labels.items()]
    kms = boto3.client("kms", region_name=region)
    try:
        response = kms.create_key(
            Description="Backmac-created key for encrypting resources in DR region",
            KeyUsage="ENCRYPT_DECRYPT",
            Origin="AWS_KMS",
            BypassPolicyLockoutSafetyCheck=False,
            Tags=kms_tags,
        )
    except Exception as e:
        raise Exception(f"Unable to create KMS Key in {AWS_REGION} region: {e}") from e
    return response["KeyMetadata"]["KeyId"]


def create_kms_alias(region: str, key_id: str, alias: str) -> AliasListEntryTypeDef:
    kms = boto3.client("kms", region_name=region)
    alias_name = alias if alias.startswith("alias/") else f"alias/{alias}"
    try:
        kms.create_alias(AliasName=alias_name, TargetKeyId=key_id)
    except Exception as e:
        raise Exception(f"Unable to create alias `{alias_name}` on kms key `{key_id}` in {region} region: {e}") from e
    # create_alias doesn't return anything, so create a partial response in the AliasListEntryTypeDef format
    return {
        "AliasName": alias_name,
        "AliasArn": f"arn:{AWS_PARTITION}:kms:{region}:{AWS_ACCOUNT_ID}:{alias_name}",
        "TargetKeyId": key_id,
    }


def create_kms_grant(region: str, key_id: str, backmac_role: str) -> CreateGrantResponseTypeDef:
    kms = boto3.client("kms", region_name=region)
    try:
        response = kms.create_grant(
            KeyId=key_id,
            GranteePrincipal=backmac_role,
            RetiringPrincipal=backmac_role,
            Operations=["Decrypt", "Encrypt", "GenerateDataKeyWithoutPlaintext"],
            Name=f"Backmac_Grant_for_{backmac_role}",
        )
    except Exception as e:
        raise Exception(f"Unable to create grant on kms key `{key_id}` in {AWS_REGION} region: {e}") from e
    return response


def get_cfn_stack_encryption_key(stack_name: str) -> str:
    stack_params = cfn.describe_stacks(StackName=stack_name)["Stacks"][0]["Parameters"]
    try:
        key_arn = next(param for param in stack_params if param["ParameterKey"] == "KmsKeyArn")["ParameterValue"]
    except StopIteration:
        print("Stack is not encrypted")
        key_arn = ""
    return key_arn


def get_ebs_encryption_key(ebs_id: str) -> str:
    volume = ec2.describe_volumes(VolumeIds=[ebs_id])
    try:
        key_arn = volume["Volumes"][0]["KmsKeyId"]
    except KeyError:
        print("EBS is not encrypted")
        key_arn = ""
    return key_arn


def get_efs_encryption_key(efs_id: str) -> str:
    filesystem = efs.describe_file_systems(FileSystemId=efs_id)
    try:
        key_arn = filesystem["FileSystems"][0]["KmsKeyId"]
    except KeyError:
        print("EFS is not encrypted")
        key_arn = ""
    return key_arn


def get_rds_encryption_key(rds_id: str) -> str:
    instance = rds.describe_db_instances(DBInstanceIdentifier=rds_id)
    try:
        key_arn = instance["DBInstances"][0]["KmsKeyId"]
    except KeyError:
        print("RDS is not encrypted")
        key_arn = ""
    return key_arn


def get_aliases_for_key(region: str, key_arn: str) -> list[str]:
    kms = boto3.client("kms", region_name=region)
    paginator = kms.get_paginator("list_aliases")
    return [alias["AliasName"] for page in paginator.paginate(KeyId=key_arn) for alias in page["Aliases"]]


def get_all_aliases(region: str) -> list[AliasListEntryTypeDef]:
    kms = boto3.client("kms", region_name=region)
    paginator = kms.get_paginator("list_aliases")
    return [alias for page in paginator.paginate() for alias in page["Aliases"]]


def lambda_handler(event, context):
    service: BackmacServiceTypeDef = event["service"]

    # create dicts of kms keys as keys, with lists of the services that use them as the value
    source_kms_key_arns: dict[str, list[Literal["ebs", "efs", "rds", "stack"]]] = {}
    dr_kms_key_arns: dict[str, list[Literal["ebs", "efs", "rds"]]] = {}

    if "ebs_id" in service:
        source_kms_key_arns.setdefault(get_ebs_encryption_key(service["ebs_id"]), []).append("ebs")

    if "efs_id" in service:
        source_kms_key_arns.setdefault(get_efs_encryption_key(service["efs_id"]), []).append("efs")

    if "rds_id" in service:
        source_kms_key_arns.setdefault(get_rds_encryption_key(service["rds_id"]), []).append("rds")

    if service.get("iac") == "cfn":
        source_kms_key_arns.setdefault(get_cfn_stack_encryption_key(service["name"]), []).append("stack")

    source_kms_key_arns.pop("", None)
    event["source_kms_key_arns"] = source_kms_key_arns

    backmac_role = get_backmac_role()

    if not source_kms_key_arns:
        print("no KMS keys found for service (EFS, RDS, CFN, etc.)")

    # retrieve the aliases for all keys in the DR region now, to avoid extra API calls in the loop below
    dr_aliases = get_all_aliases(event["dr_region"])

    for key_arn, aws_services in source_kms_key_arns.items():
        # grant ourselves permission to use this key in this region
        create_kms_grant(AWS_REGION, key_arn, backmac_role)

        # get all the aliases for this key in this region
        key_aliases = get_aliases_for_key(AWS_REGION, key_arn)

        # search the DR region for any of those same aliases
        matching_dr_aliases = [dr_alias for dr_alias in dr_aliases if dr_alias["AliasName"] in key_aliases]

        # if we didn't find any, create a new key in the DR region and create aliases of the same name(s) for that key
        if not matching_dr_aliases:
            new_dr_key_id = create_kms_key(event["dr_region"], service)
            for alias in key_aliases:
                matching_dr_aliases.append(create_kms_alias(event["dr_region"], new_dr_key_id, alias))

        for alias in matching_dr_aliases:
            # use full ARN
            dr_kms_key_arn = f"arn:{AWS_PARTITION}:kms:{event['dr_region']}:{AWS_ACCOUNT_ID}:key/{alias['TargetKeyId']}"

            # grant ourselves permission to use the DR key(s) in the DR region
            create_kms_grant(event["dr_region"], dr_kms_key_arn, backmac_role)

            # add the key to our list of DR keys with the list of aws_services used by this key in the source region
            dr_kms_key_arns.setdefault(dr_kms_key_arn, [])
            for aws_service in aws_services:
                if aws_service not in dr_kms_key_arns[dr_kms_key_arn]:
                    dr_kms_key_arns[dr_kms_key_arn].append(aws_service)
                # since we use EFS in source regions and EBS in DR, make sure this key is usable for EBS resources
                # in DR region if this key is used for EFS in the source region
                if aws_service == "efs" and "ebs" not in dr_kms_key_arns[dr_kms_key_arn]:
                    dr_kms_key_arns[dr_kms_key_arn].append("ebs")

    event["dr_kms_key_arns"] = dr_kms_key_arns

    return event
