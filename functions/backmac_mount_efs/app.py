#!/usr/bin/env python3

from __future__ import annotations

from typing import TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, ssm_wait_response

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance

ec2 = boto3.resource("ec2", config=BOTO_CONFIG)
efs = boto3.client("efs", config=BOTO_CONFIG)


def get_efs_security_group(efs_id: str) -> str:
    mount_targets = efs.describe_mount_targets(FileSystemId=efs_id)["MountTargets"]
    security_group = efs.describe_mount_target_security_groups(MountTargetId=mount_targets[0]["MountTargetId"])[
        "SecurityGroups"
    ][0]
    return security_group


def add_security_group(backmac_instance: Instance, group_id: str) -> None:
    print(f"Adding security group {group_id} to backmac instance {backmac_instance.instance_id}")
    grouplist = [d["GroupId"] for d in backmac_instance.security_groups]
    if group_id not in grouplist:
        grouplist.append(group_id)
        backmac_instance.modify_attribute(Groups=grouplist)


def get_used_space(efs_id: str) -> int:
    """adding used space from /media/atl to event for debugging and resizing if required"""
    efs_details = efs.describe_file_systems(FileSystemId=efs_id)
    used_space = efs_details["FileSystems"][0]["SizeInBytes"]
    return int(used_space["Value"] / 1000)


def check_mount_target(backmac_instance: Instance, efs_id: str) -> None:
    # TODO make sure the efs has a valid mount target in this AZ, if not, create one.
    mount_targets = efs.describe_mount_targets(FileSystemId=efs_id)
    mount_targets_subnets = list(map(lambda target: target["SubnetId"], mount_targets["MountTargets"]))
    print(f'subnets for {efs_id} mount targets: {", ".join(mount_targets_subnets)}')

    if backmac_instance.subnet_id in mount_targets_subnets:
        print("found subnet in mount targets")
    else:
        print("couldn't find subnet in mount targets, need to create")
        raise Exception(f'No EFS mount target found in {backmac_instance.placement["AvailabilityZone"]}')


def lambda_handler(event, context):
    backmac_instance = ec2.Instance(event["backmac_instance"])

    efs_id: str = event["service"]["efs_id"]
    security_group_id = get_efs_security_group(efs_id)
    event["efs_security_group_id"] = security_group_id

    check_mount_target(backmac_instance, efs_id)
    add_security_group(backmac_instance, security_group_id)

    print(f'Mounting efs {efs_id} to backmac instance {event["backmac_instance"]}')
    mount_cmd = f"mount -t efs -o tls,ro {efs_id}:/ /media/atl"
    ssm_wait_response(backmac_instance, mount_cmd)

    event["efs_used_space"] = get_used_space(efs_id)
    return event
