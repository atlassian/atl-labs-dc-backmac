#!/usr/bin/env python3

from __future__ import annotations

import boto3

from backmac_utils import BOTO_CONFIG

SSM = boto3.client("ssm", config=BOTO_CONFIG)


def ssm_check_command(rsync_command_id: str) -> str:
    rsync_command = SSM.list_commands(CommandId=rsync_command_id)["Commands"][0]
    if rsync_command["Status"] == "Success":
        s3_uri = "/".join([rsync_command["OutputS3BucketName"], rsync_command["OutputS3KeyPrefix"]])
        print(f"parallel_sync completed successfully; logs available at s3://{s3_uri}/")
    return rsync_command["Status"]


def lambda_handler(event, context):
    rsync_command_id: str = event["rsync_command_id"]

    if rsync_command_id:
        event["rsync_status"] = ssm_check_command(rsync_command_id)
    else:
        event["rsync_status"] = "Failed"

    print(("rsync status is: " + event["rsync_status"]))
    return event
