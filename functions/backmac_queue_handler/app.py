#!/usr/bin/env python3

from __future__ import annotations

import json
import logging
import os
from typing import TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, get_service_resource_id, get_service_tags_or_labels

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef
    from mypy_boto3_stepfunctions.type_defs import StartExecutionOutputTypeDef
    from mypy_boto3_stepfunctions.literals import RegionName

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_REGION = os.environ["AWS_REGION"]
POSSIBLE_DATABASE_TYPES: set[str] = {"rds_id"}
POSSIBLE_STORAGE_TYPES: set[str] = {"ebs_id", "efs_id"}

cfn = boto3.client("cloudformation", config=BOTO_CONFIG)
dynamodb = boto3.resource("dynamodb", config=BOTO_CONFIG)
ec2 = boto3.client("ec2", config=BOTO_CONFIG)
sfn = boto3.client("stepfunctions", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_bitbucket_mesh_vols(service: BackmacServiceTypeDef) -> list[str]:
    paginator = cfn.get_paginator("describe_stacks")
    stacks = [stack for page in paginator.paginate() for stack in page["Stacks"]]
    # find the stack ID of the mesh stack which specifies this service as it's "BitbucketPrimaryStack" CFN param
    try:
        mesh_stack_id = next(
            stack["StackId"]
            for stack in stacks
            if "Parameters" in stack
            and {"ParameterKey": "BitbucketPrimaryStack", "ParameterValue": service["name"]} in stack["Parameters"]
        )
    except StopIteration as e:
        raise Exception(
            f"Unable to find mesh stack with parameter BitbucketPrimaryStack with value matching service: {service}"
        ) from e
    # find the EC2 instances tagged with the corresponding stack ID
    mesh_ec2_instances = [
        reservation["Instances"][0]
        for reservation in ec2.describe_instances(
            Filters=[{"Name": "tag:aws:cloudformation:stack-id", "Values": [mesh_stack_id]}]
        )["Reservations"]
    ]
    # find the EBS volume IDs for the block devices mounted at /dev/xvdd on the corresponding EC2 instances
    mesh_ebs_vol_ids = [
        device["Ebs"]["VolumeId"]
        for instance in mesh_ec2_instances
        for device in instance["BlockDeviceMappings"]
        if device["DeviceName"] == "/dev/xvdd"
    ]
    return mesh_ebs_vol_ids


def start_backup_run(sfn_name: str, execution_name: str, sfn_input: dict) -> StartExecutionOutputTypeDef:
    sfn_input_str = json.dumps(sfn_input)
    logger.info(f"SFN input: {sfn_input_str}")
    execution_args = {
        "stateMachineArn": f"arn:aws:states:{AWS_REGION}:{AWS_ACCOUNT_ID}:stateMachine:{sfn_name}",
        "input": sfn_input_str,
        "name": execution_name,
    }
    sfn_run = sfn.start_execution(**execution_args)
    return sfn_run


def lambda_handler(event: dict, context):
    backup_history = dynamodb.Table(os.environ["backup_history_table_name"])
    batch_item_failure_message_ids: list[str] = []

    messages = event.get("Records", [])
    logger.info(f"Received {len(messages)} SQS messages for processing")

    for message in messages:
        logger.info(f"Processing message: {message}")

        service: BackmacServiceTypeDef = json.loads(message["body"])
        backup_machine: str = message["messageAttributes"]["backup_machine"]["stringValue"]
        dr_region: RegionName = message["messageAttributes"]["dr_region"]["stringValue"]
        retry_count = int(message["messageAttributes"]["retry_count"]["stringValue"])
        message_group_id = message["attributes"]["MessageGroupId"]
        run_type = message["messageAttributes"]["run_type"]["stringValue"]

        if retry_count > 2:
            logger.error(f"Exceeded maximum number of retries for service {service}")
            batch_item_failure_message_ids.append(message["messageId"])
            continue

        # if we have a cfn/k8s service, check that it exists by retrieving its tags/labels
        if "iac" in service:
            try:
                service["labels"] = get_service_tags_or_labels(service)
            except Exception:
                logger.exception("Unable to retrieve service tags/labels")
                batch_item_failure_message_ids.append(message["messageId"])
                continue
            else:
                service["clone"] = "cloned_from" in service["labels"]

        # if we don't already know the RDS name for this service, go find it
        if not any(storage_type in service for storage_type in POSSIBLE_DATABASE_TYPES):
            # rds is the only option currently for all infrastructures
            try:
                service["rds_id"] = get_service_resource_id(service, "rds")
            except Exception:
                # some stacks like bitbucket mesh don't have an RDS instance and are automatically handled from the
                # primary stack
                logger.exception(f"Unable to retrieve rds_id for service {service}")
                batch_item_failure_message_ids.append(message["messageId"])
                continue

        # if we don't already know the type and ID of the shared_home storage, go find it
        if not any(storage_type in service for storage_type in POSSIBLE_STORAGE_TYPES):
            try:
                if service["iac"] == "cfn" and service["clone"]:
                    service["ebs_id"] = get_service_resource_id(service, "ebs")
                # TODO: add find_k8s_persistent_storage_type method for looking up non-EFS persistent storage
                # in-use by pod(s)? and add custom resource definition to backmac_utils.get_k8s_resource_id
                else:
                    service["efs_id"] = get_service_resource_id(service, "efs")
            except Exception:
                logger.exception(f"Unable to retrieve efs_id or ebs_id for service {service}")
                batch_item_failure_message_ids.append(message["messageId"])
                continue

        # keep track of which EBS volumes we'll need to snapshot later
        ebs_vols_to_snap = []
        if "ebs_id" in service:
            ebs_vols_to_snap.append(service["ebs_id"])

        # do product-specific things
        try:
            product = service["labels"]["product"].lower()
        except KeyError:
            logger.warn(f"Unable to determine product type for service: {service}")
        else:
            if product == "bitbucket":
                try:
                    ebs_vols_to_snap.extend(get_bitbucket_mesh_vols(service))
                except Exception:
                    logger.exception(f"Unable to retrieve mesh volume IDs for service: {service}")
                    batch_item_failure_message_ids.append(message["messageId"])
                    continue

        execution_name_parts = [message_group_id]
        execution_name_parts.extend([service[param] for param in ["name", "iac"] if param in service])
        if len(execution_name_parts) == 1:
            execution_name_parts.extend([v for k, v in service.items() if k.endswith("_id")])
        execution_name_parts.append(run_type)
        execution_name = "-".join(execution_name_parts)
        sqs_message_time = int(int(message["attributes"]["SentTimestamp"]) / 1000)  # convert micro to milli

        sfn_input = {
            "dr_region": dr_region,
            "ebs_vols_to_snap": ebs_vols_to_snap,
            "retry_count": retry_count,
            "run_type": run_type,
            "service": service,
            "sqs_details": {
                "message_id": message["messageId"],
                "message_group_id": message_group_id,
                "queue_name": message["eventSourceARN"].split(":")[-1],
                "sender_id": message["attributes"]["SenderId"],
                "sent_timestamp": sqs_message_time,
            },
        }

        # for each of these attributes, if we received them, pass them to the SFN event input
        for attr_name in {"instance_capacity_type", "instance_size", "webhook_notification_url"}:
            if attr_name in message["messageAttributes"]:
                sfn_input[attr_name] = message["messageAttributes"][attr_name]["stringValue"]

        logger.info(f"Starting backup processing for {execution_name}")
        try:
            exec_details = start_backup_run(sfn_name=backup_machine, execution_name=execution_name, sfn_input=sfn_input)
        except Exception:
            logger.exception(f"Unable to start state machine {backup_machine} for service {service}")
            batch_item_failure_message_ids.append(message["messageId"])
        else:
            logger.info(f"Execution {exec_details['executionArn']} started for service {service}")
            update_expressions = [
                "sqs_receive_time=:r",
                "execution_name=:n",
                "expiration_time=:e",
            ]
            update_expression_attribute_values = {
                ":r": sqs_message_time,
                ":n": execution_name,
                ":e": sqs_message_time + (7776000),  # keep 90d of history
            }
            if "name" in service:
                update_expressions.append("service_name=:s")
                update_expression_attribute_values[":s"] = service["name"]
            backup_history.update_item(
                Key={"sqs_message_id": message["messageId"]},
                UpdateExpression=f"SET {', '.join(update_expressions)}",
                ExpressionAttributeValues=update_expression_attribute_values,  # type: ignore
            )

    return {"batchItemFailures": [{"itemIdentifier": message_id} for message_id in batch_item_failure_message_ids]}
