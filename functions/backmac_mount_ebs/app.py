#!/usr/bin/env python3

from __future__ import annotations

import logging
import time

from typing import TYPE_CHECKING

import boto3
import botocore

from backmac_exceptions import SsmExecutionError
from backmac_utils import BOTO_CONFIG, get_backup_volume, ssm_wait_response

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance, Volume

ec2 = boto3.resource("ec2", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def attach_volume(backmac_instance: Instance, backup_volume: Volume) -> None:
    device_name = "/dev/xvdz"
    try:
        backup_volume.attach_to_instance(Device=device_name, InstanceId=backmac_instance.id)
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "VolumeInUse":
            if backup_volume.attachments[0]["InstanceId"] == backmac_instance.id:
                logger.info(
                    f"backup volume {backup_volume.volume_id} is already attached to the correct instance ({backmac_instance.instance_id}); nothing to do"
                )
            else:
                raise Exception(
                    f"backup volume {backup_volume.volume_id} is attached to {backup_volume.attachments[0]['InstanceId']}, which is not the backmac instance for this run (should be {backmac_instance.id})!"
                )
        else:
            logger.exception("Unhandled ClientError while attempting to attach backup volume")
            raise
    # wait for the volume to be attached
    waiter = ec2.meta.client.get_waiter("volume_in_use")
    waiter.wait(VolumeIds=[backup_volume.volume_id])
    # ensure the device is available to the OS
    while True:
        file_response = ssm_wait_response(backmac_instance, f"file -bsL {device_name}")
        if "cannot open" in file_response:
            time.sleep(2)
        else:
            break


def ensure_volume_filesystem(backmac_instance: Instance, backup_volume: Volume) -> str:
    device_name = backup_volume.attachments[0]["Device"]
    try:
        filesystem_type = ssm_wait_response(backmac_instance, f"blkid {device_name} -o value -s TYPE")
        logger.info(f"Found filesystem type: {filesystem_type}")
    except SsmExecutionError as e:
        if e.return_code == 2:
            filesystem_type = "xfs"
            logger.info(f"volume {backup_volume.volume_id} has no filesystem - creating {filesystem_type}")
            mkfs_response = ssm_wait_response(backmac_instance, f"mkfs -t {filesystem_type} {device_name}")
            logger.info(mkfs_response)
        else:
            raise
    return filesystem_type


def resize_fs(backmac_instance: Instance, device: str, filesystem_type: str) -> None:
    """This attempts to resize the filesystem to it's maximum size. It's needed after a volume resize,
    and won't do any damage if the filesystem is already at its maximum size.
    """
    match filesystem_type:
        case "xfs":
            resize_command = "xfs_growfs -d"
            repair_command = "xfs_repair"
        case "ext4":
            resize_command = "resize2fs"
            repair_command = "e2fsck -y -f "
        case _:
            raise Exception(f"Unsupported filesystem type for resize: {filesystem_type}")
    try:
        ssm_wait_response(backmac_instance, f"{resize_command} {device}")
    except SsmExecutionError as resize_err:
        if resize_err.return_code == 1:
            # resize failed, try repair first
            try:
                ssm_wait_response(backmac_instance, f"{repair_command} {device}")
            except SsmExecutionError as repair_err:
                # e2fsck returns 1 when the filesystem was modified; xfs_repair returns 0 even when the filesystem was
                # modified without problems... so only raise for other exit codes when we're using e2fsck
                if "fsck" in repair_command and repair_err.return_code == 1:
                    pass
                else:
                    raise
            ssm_wait_response(backmac_instance, f"{resize_command} {device}")


def mount_volume(backmac_instance: Instance, backup_volume: Volume) -> None:
    try:
        ssm_wait_response(backmac_instance, f"mount {backup_volume.attachments[0]['Device']} /backup")
    except SsmExecutionError as e:
        if e.return_code == 32:
            logger.info(f"volume {backup_volume.volume_id} is already mounted; nothing to do")
        else:
            raise


def get_volume_size(backmac_instance: Instance) -> int:
    total_space = ssm_wait_response(backmac_instance, "df /backup | tail -n 1 | awk '{print $2}'")
    return int(total_space)


def get_ebs_used_percent(backmac_instance: Instance) -> int:
    total_used_percent = ssm_wait_response(backmac_instance, "df /backup | tail -n 1 | awk '{print $5}' | sed 's/%//'")
    return int(total_used_percent)


def lambda_handler(event, context):
    try:
        backmac_instance = ec2.Instance(event["backmac_instance"])
    except Exception:
        raise Exception("No backmac_instance found in event")

    backup_volume = (
        ec2.Volume(event["ebs_backup_vol"])
        if "ebs_backup_vol" in event
        else get_backup_volume(event["service"]["name"])
    )

    # collect some current state stuff for diagnostics if fail
    logger.info(ssm_wait_response(backmac_instance, "df -h; lsblk; nvme list"))

    # attach the volume
    attach_volume(backmac_instance, backup_volume)

    # ensure mount destination exists on instance
    ssm_wait_response(backmac_instance, "mkdir -p /backup")

    # ensure backup volume has a file system
    filesystem_type = ensure_volume_filesystem(backmac_instance, backup_volume)

    # mount the backup volume
    mount_volume(backmac_instance, backup_volume)

    # resize the filesystem, if needed
    resize_fs(backmac_instance, backup_volume.attachments[0]["Device"], filesystem_type)

    # if we got this far, store some backup volume info on event
    event["backup_vol_device_name"] = backup_volume.attachments[0]["Device"]
    event["backup_vol_filesystem"] = filesystem_type
    event["ebs_backup_vol"] = backup_volume.volume_id
    event["ebs_size"] = get_volume_size(backmac_instance)
    event["ebs_used_percent"] = get_ebs_used_percent(backmac_instance)

    # if we were thrown back into the waiter after resizing the volume, the list might already have the backup volume
    if backup_volume.volume_id not in event["ebs_vols_to_snap"]:
        event["ebs_vols_to_snap"].append(backup_volume.volume_id)

    # Resize if the EBS is not at least 15% larger than the EFS or EBS used space is greater than 90%
    if (event["ebs_size"] < event["efs_used_space"] * 1.15) or event["ebs_used_percent"] >= 90:
        event["resize_required"] = "True"
    else:
        event["resize_required"] = "False"
    return event
