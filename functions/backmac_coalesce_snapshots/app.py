#!/usr/bin/env python3


def lambda_handler(event, context):
    return event | {"snapshots": [*event.pop("ebs_snapshots"), *event.pop("rds_snapshots")]}
