#!/usr/bin/env python3

from __future__ import annotations

import os

from datetime import datetime, timedelta
from typing import TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, clean_labels, get_backup_retention, labels_to_tags, tags_to_labels

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef

cfn = boto3.client("cloudformation", config=BOTO_CONFIG)
ec2 = boto3.resource("ec2", config=BOTO_CONFIG)

AWS_REGION = os.environ["AWS_REGION"]


def lambda_handler(iterator, context):
    event = iterator["event"]
    sfn_context = iterator["sfn_context"]

    service: BackmacServiceTypeDef = event["service"]
    ebs_vol = ec2.Volume(iterator["ebs_vol_id"])

    ebs_vol_labels = tags_to_labels(ebs_vol.tags)

    timestamp = datetime.now()
    backup_retention = get_backup_retention(service)
    snap_expiry = timestamp + timedelta(backup_retention)
    # set snap name based on service_name tag on EBS volume (with fallback to service name, and to volume ID if that
    # doesn't exist); this ensures mesh volume snapshots for Bitbucket stacks have the name of their stack and not the
    # primary stack, and any snapshots for arbitary EBS volumes refer to the volume ID if that volume has no tagging
    snap_name = f"{ebs_vol_labels.get('service_name', service.get('name', ebs_vol.id))}_ebs_snap_{timestamp.strftime('%Y%m%d%H%M')}"

    snap_description: list[str] = ["snapshot of"]
    snap_description.append(service.get("name"))
    snap_description.append(ebs_vol_labels.get("ebs_vol_name", ebs_vol.id))
    snap_description.append(f"taken at {timestamp.strftime('%Y%m%d%H%M')}")

    snap_labels: dict[str, str] = {}
    snap_labels = snap_labels | clean_labels(service.get("labels", {}))
    snap_labels = snap_labels | clean_labels(ebs_vol_labels)
    snap_labels = snap_labels | {
        "backup_date": str(timestamp),
        "backup_delete_after": str(snap_expiry),
        "backup_lambda_log_group_name": f"{os.getenv('AWS_LAMBDA_LOG_GROUP_NAME', '')}",
        "backup_lambda_log_stream_guid": f"{os.getenv('AWS_LAMBDA_LOG_STREAM_NAME', '').split(']')[-1]}",
        "backup_machine": sfn_context["StateMachine"]["Name"],
        "backup_run_id": sfn_context["Execution"]["Name"],
        "created_by": "backmac",
        "Name": snap_name,
        "source_vol_id": ebs_vol.volume_id,
    }
    if "name" in service:
        snap_labels["service_name"] = service["name"]
    snap_tags = labels_to_tags(snap_labels)

    ebs_snap = ebs_vol.create_snapshot(
        Description=" ".join([segment for segment in snap_description if segment]),
        TagSpecifications=[{"ResourceType": "snapshot", "Tags": snap_tags}],
    )

    return {
        "id": ebs_snap.snapshot_id,
        "region": AWS_REGION,
        "type": "ebs",
        "status": ebs_snap.state,
    }
