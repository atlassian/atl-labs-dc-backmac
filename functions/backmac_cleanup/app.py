#!/usr/bin/env python3

from __future__ import annotations

import pprint

import boto3
import botocore

from backmac_exceptions import SsmExecutionError
from backmac_utils import BOTO_CONFIG, ssm_wait_response
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance

EC2 = boto3.resource("ec2", config=BOTO_CONFIG)
SSM = boto3.client("ssm", config=BOTO_CONFIG)


def get_mounted_backup_volume_id(backmac_instance: Instance):
    backup_volume_id = None
    print(f"looking for a backup volume mounted to {backmac_instance.id}")
    backmac_device_details = list(
        filter(
            lambda device: device["DeviceName"] == "/dev/xvdz",
            backmac_instance.block_device_mappings,
        )
    )
    try:
        backup_volume_id = backmac_device_details[0]["Ebs"]["VolumeId"]
    except IndexError:
        print("nothing mounted to backmac on /dev/xvdz")
    return backup_volume_id


def detach_volume(backmac_instance: Instance, backup_volume_id: str):
    # backup device is always attached as /dev/xvdz so lets detach that on cleanup
    print(f"detaching volume /dev/xvdz from {backmac_instance.id}")
    try:
        backmac_instance.detach_volume(
            Device="/dev/xvdz",
            Force=True,
            VolumeId=backup_volume_id,
        )
    except Exception as e:
        print("type is:", e.__class__.__name__)
        pprint.pprint(e)
        return "failed"


def lambda_handler(event, context):
    try:
        backmac_instance = EC2.Instance(event["backmac_instance"])
        print(f"cleaning up instance {backmac_instance.id}; state: {backmac_instance.state['Name']}")
    except (KeyError, botocore.exceptions.ClientError):
        # there's no backmac_instance; nothing past this point matters
        print("No backmac_instance found in event; nothing to clean")
        return event

    if backmac_instance.state["Name"] == "running":
        try:
            ssm_wait_response(backmac_instance, "umount -f /backup", exec_timeout=60)
            print("unmounting /backup completed successfully")
        except SSM.exceptions.InvalidInstanceId:
            print("instance is in an invalid state; skipping unmount and continuing with cleanup")
        except SsmExecutionError as e:
            if e.return_code == 32:
                print("/backup is already unmounted; nothing to do")
            if e.return_code == 137:
                # we'll hit this if the SSM command takes longer than the exec_timeout; surface the error for visibilty
                # but re-raise the exception so the state machine can retry this function (up to the configured number)
                print("unmounting /backup timed out!")
                raise
            else:
                print(f"unmounting /backup failed ({e.return_code}); continuing with cleanup")

        try:
            # we don't need a clean unmount here—lazy is fine, and the EFS is mounted read-only anyways—we just need to
            # keep the instance termination from hanging if the EFS mount doesn't want to let go
            ssm_wait_response(backmac_instance, "umount -l /media/atl", exec_timeout=60)
            print("unmounting /media/atl completed successfully")
        except SSM.exceptions.InvalidInstanceId:
            print("instance is in an invalid state; skipping unmount and continuing with cleanup")
        except SsmExecutionError as e:
            if e.return_code == 32:
                print("/media/atl is already unmounted; nothing to do")
            if e.return_code == 137:
                # we'll hit this if the SSM command takes longer than the exec_timeout; surface the error for visibilty
                # but re-raise the exception so the state machine can retry this function (up to the configured number)
                print("unmounting /media/atl timed out!")
                raise
            else:
                print(f"unmounting /media/atl failed ({e.return_code}); continuing with cleanup")
        except Exception:
            print("Something else has gone wrong unmounting /media/atl, continue anyway")

    # look for mounted backup volume id
    backup_volume_id = get_mounted_backup_volume_id(backmac_instance)
    if backup_volume_id:
        # detach EBS
        detach_volume(backmac_instance, backup_volume_id)

    # terminate the instance (by deleting the fleet)
    print(f"deleting fleet id {event['backmac_fleet_id']}")
    print(EC2.meta.client.delete_fleets(FleetIds=[event["backmac_fleet_id"]], TerminateInstances=True))

    # if we failed while efs2ebs was in-progress, try to cancel the command
    try:
        if all(key in event for key in {"backmac_instance", "rsync_command_id"}):
            SSM.cancel_command(CommandId=event["rsync_command_id"], InstanceIds=[event["backmac_instance"]])
    except (SSM.exceptions.InvalidCommandId, SSM.exceptions.InvalidInstanceId):
        pass

    return event
