#!/usr/bin/env python3

from __future__ import annotations

import json
import logging
import time

from typing import TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, substring_in_dict

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef
    from mypy_boto3_sqs.type_defs import MessageAttributeValueTypeDef

SQS = boto3.resource("sqs", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(params, context):
    event = params["event"]
    sfn_context = params["sfn_context"]

    service: BackmacServiceTypeDef = event["service"]

    # only requeue services if they were part of a scheduled/queued run
    # i.e., don't auto-requeue runs started via API (let API callers manage failures on their own)
    if event["run_type"] == "api":
        logger.warn("Current run was triggered via API; skipping retry")
        return event

    retry_count = int(event["retry_count"]) + 1

    msg_attributes: dict[str, MessageAttributeValueTypeDef] = {
        "backup_machine": {"DataType": "String", "StringValue": sfn_context["StateMachine"]["Name"]},
        "dr_region": {"DataType": "String", "StringValue": event["dr_region"]},
        "retry_count": {"DataType": "Number.int", "StringValue": str(retry_count)},
        "run_type": {"DataType": "String", "StringValue": "retry"},
    }

    # SQS doesn't allow empty attributes
    # only include the following params in the requeue message if this execution received them and they have a value
    for attr_name in {"instance_capacity_type", "instance_size", "webhook_notification_url"}:
        attr_val = event.get(attr_name, "").strip()
        if attr_val:
            msg_attributes[attr_name] = {
                "DataType": "String",
                "StringValue": attr_val,
            }

    # if the error indicates a spot instance interruption, queue an on-demand run
    try:
        if substring_in_dict(event, "Server.SpotInstanceTermination"):
            msg_attributes["instance_capacity_type"] = {
                "DataType": "String",
                "StringValue": "on-demand",
            }
    except Exception:
        pass

    queue = SQS.get_queue_by_name(QueueName=event["sqs_details"]["queue_name"])

    try:
        queue_response = queue.send_message(
            MessageBody=json.dumps(service),
            MessageAttributes=msg_attributes,
            MessageDeduplicationId=str(time.time()),  # break the FIFO deduplication for retries
            MessageGroupId=event["sqs_details"]["message_group_id"],
        )
    except Exception as e:
        logger.exception(f"Failure sending message to the queue: {e}")
        raise
    else:
        logger.info(f"Response from queue: {queue_response}")

    return event
