#!/usr/bin/env python3

from __future__ import annotations

import copy
import json
import logging
import os
import time

import boto3
from datetime import datetime
from typing import Literal, Optional, TYPE_CHECKING

import backmac_notify as notify
from backmac_utils import BOTO_CONFIG

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef
    from mypy_boto3_sqs.service_resource import Queue
    from mypy_boto3_sqs.type_defs import MessageAttributeValueTypeDef

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
POSSIBLE_DATABASE_TYPES: set[str] = {"rds_id"}
POSSIBLE_STORAGE_TYPES: set[str] = {"ebs_id", "efs_id"}

dynamodb = boto3.resource("dynamodb", config=BOTO_CONFIG)
sqs = boto3.resource("sqs", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    # we might receive an event from API Gateway or SQS... if this is API Gateway, retrieve the
    # "event" from the request body and replace the event var with that data
    try:
        is_api_event = "resourceId" in event["requestContext"]
    except KeyError:
        is_api_event = False
    else:
        api_event = copy.deepcopy(event)
        event = json.loads(api_event["body"])
        if "webhook_notification_url" not in event:
            # if we don't do this, API-invoked backmac runs would never send notifications unless the
            # request body included the "webhook_notification_url" param, and we don't necessarily
            # want to share our webhook URLs with others... they can still supply their own, but by
            # default we'll get notified in our default channel for these runs
            event["webhook_notification_url"] = os.environ["default_webhook_notification_url"]

    event["run_type"] = "api" if is_api_event else "manual"

    # try to pull standard inputs from the event; use sensible fallbacks
    backup_machine: str = event["backup_machine"] if "backup_machine" in event else "backup_machine"
    dr_region: str = event["dr_region"] if "dr_region" in event else os.environ["dr_region"]
    queue_name: str = event["queue_name"] if "queue_name" in event else os.environ["queue_name"]

    # check to see if a service name and iac was passed in the event; fallback for iac is hardcoded
    # to 'cfn'; we only use this if service_name or stack_name was provided alone and allows for
    # backwards compatibility with existing invocation patterns for single-stack runs (i.e., single-
    # stack runs for k8s services is not yet supported)
    # TODO: when only a service name is passed, search CFN or kubernetes cluster for matching service
    # before queueing
    service_name: Optional[str]
    if "stack_name" in event:
        service_name = event.pop("stack_name")
    elif "service_name" in event:
        service_name = event.pop("service_name")
    else:
        service_name = None

    service_iac: Literal["cfn", "k8s"] = event["service_iac"] if "service_iac" in event else "cfn"
    service: BackmacServiceTypeDef = {"name": service_name, "iac": service_iac} if service_name else None

    # if no service was specified, check to see if specific EFS and RDS were specified
    if not service:
        found_database_id_params = event.keys() & POSSIBLE_DATABASE_TYPES
        found_storage_id_params = event.keys() & POSSIBLE_STORAGE_TYPES
        if len(found_database_id_params) == 1 and len(found_storage_id_params) == 1:
            database_id_param = found_database_id_params.pop()
            database_id = event.pop(database_id_param)
            storage_id_param = found_storage_id_params.pop()
            storage_id = event.pop(storage_id_param)
            logger.info(f'Found {database_id_param} "{database_id}" and {storage_id_param} "{storage_id}" in event')
            service: BackmacServiceTypeDef = {
                database_id_param: database_id,
                storage_id_param: storage_id,
            }
        elif len(found_storage_id_params) > 1 or len(found_storage_id_params) > 1:
            raise Exception("Found more than one possible database or storage ID for backup!")

    service_list: list[BackmacServiceTypeDef] = []
    if service:
        service_list.append(service)
    else:
        raise Exception("No service or compatible database/storage types provided")

    msg_attributes: dict[str, MessageAttributeValueTypeDef] = {
        "backup_machine": {"DataType": "String", "StringValue": backup_machine},
        "dr_region": {"DataType": "String", "StringValue": dr_region},
        "retry_count": {"DataType": "Number.int", "StringValue": "0"},
        "run_type": {"DataType": "String", "StringValue": event["run_type"]},
    }

    # SQS doesn't allow empty attributes
    # only include the following params in the message if we received them and they have a value
    for attr_name in {"instance_capacity_type", "instance_size", "webhook_notification_url"}:
        attr_val = event.get(attr_name, "").strip()
        if attr_val:
            msg_attributes[attr_name] = {
                "DataType": "String",
                "StringValue": attr_val,
            }

    backup_history = dynamodb.Table(os.environ["backup_history_table_name"])
    queue: Queue = sqs.get_queue_by_name(QueueName=queue_name)
    api_response = {}
    status_code = 202

    try:
        queue_response = queue.send_message(
            MessageBody=json.dumps(service),
            MessageAttributes=msg_attributes,
            MessageGroupId=datetime.utcnow().strftime("%Y%m%d-%H%M%S-%f"),
        )
    except Exception:
        logger.exception("Failure sending message to the queue")
        raise
    else:
        logger.info(f"Response from queue: {queue_response}")
        api_response["backup_id"] = queue_response["MessageId"]
        history_item = backup_history.get_item(Key={"sqs_message_id": queue_response["MessageId"]}, ConsistentRead=True)
        service_or_volumes_name = (
            f"service {service['name']}"
            if "name" in service
            else ", ".join([f"{k}: {v}" for k, v in service.items() if k.endswith("_id")])
        )
        if "Item" in history_item:
            # we found the message ID in the history; SQS will consider this message a duplicate
            dupe_message_id = history_item["Item"]["sqs_message_id"]
            existing_execution_name = history_item["Item"].get("execution_name")
            logger.warn(f"Found existing message ID in history ({dupe_message_id}); SQS will not deliver this message")
            logger.info(f"Execution already exists for ID {dupe_message_id}: {existing_execution_name}")
            lambda_response = (
                f"Execution for {service_or_volumes_name} already started within last ~5m; will not be re-queued"
            )
            if existing_execution_name:
                api_response["backup_name"] = existing_execution_name
            status_code = 409
        else:
            # this should not be a duplicate; add message ID to the history with a short expiration time
            # queue_handler will update this item with an execution ID, SQS receipt time, and a longer expiration time
            backup_history.put_item(
                Item={
                    "sqs_message_id": queue_response["MessageId"],
                    # drop this record in 1h if the queue handler never updates it
                    "expiration_time": int(time.time()) + 3600,
                    "sfn_result": "PENDING",
                }
            )
            lambda_response = f"Added {service_or_volumes_name} to the queue for backup"

    logger.info(lambda_response)
    notify.send("info", lambda_response, event)
    api_response["message"] = lambda_response

    # though we've always returned the event below, nothing has expected that "return" since this lambda usually just
    # starts the run and then exits, but the API _does_ expect a response, so give it one if the API requested this run
    if is_api_event:
        return {"statusCode": status_code, "body": json.dumps(api_response)}

    return event
