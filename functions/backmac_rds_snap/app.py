#!/usr/bin/env python3

from __future__ import annotations

import os
from datetime import datetime, timedelta
from typing import TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, clean_labels, get_backup_retention, labels_to_tags, tags_to_labels

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef

rds = boto3.client("rds", config=BOTO_CONFIG)

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_REGION = os.environ["AWS_REGION"]


def lambda_handler(params, context):
    event = params["event"]
    sfn_context = params["sfn_context"]

    service: BackmacServiceTypeDef = event["service"]
    rds_name: str = event["service"]["rds_id"]
    rds_arn = f"arn:aws:rds:{AWS_REGION}:{AWS_ACCOUNT_ID}:db:{rds_name}"
    rds_labels = tags_to_labels(rds.list_tags_for_resource(ResourceName=rds_arn)["TagList"])

    timestamp = datetime.now()
    backup_retention = get_backup_retention(service)
    snap_expiry = timestamp + timedelta(backup_retention)
    snap_name = f"{rds_name}-snap-{timestamp.strftime('%Y%m%d%H%M')}"

    snap_labels: dict[str, str] = {}
    snap_labels = snap_labels | clean_labels(service.get("labels", {}))
    snap_labels = snap_labels | clean_labels(rds_labels)
    snap_labels = snap_labels | {
        "backup_date": str(timestamp),
        "backup_delete_after": str(snap_expiry),
        "backup_lambda_log_group_name": f"{os.getenv('AWS_LAMBDA_LOG_GROUP_NAME', '')}",
        "backup_lambda_log_stream_guid": f"{os.getenv('AWS_LAMBDA_LOG_STREAM_NAME', '').split(']')[-1]}",
        "backup_machine": sfn_context["StateMachine"]["Name"],
        "backup_run_id": sfn_context["Execution"]["Name"],
        "created_by": "backmac",
        "Name": snap_name,
        "source_rds": rds_name,
    }
    if "name" in service:
        snap_labels["service_name"] = service["name"]
    snap_tags = labels_to_tags(snap_labels)

    # RDS availability pre-check done via backmac_rds_status.
    rds_snap = rds.create_db_snapshot(DBInstanceIdentifier=rds_name, DBSnapshotIdentifier=snap_name, Tags=snap_tags)[
        "DBSnapshot"
    ]

    # add rds snap details to event
    event["rds_snapshots"] = [
        {
            "id": rds_snap["DBSnapshotIdentifier"],
            "region": AWS_REGION,
            "type": "rds",
            "status": rds_snap["Status"],
            "db_engine": rds_snap["Engine"],
            "db_engine_version": rds_snap["EngineVersion"],
        }
    ]

    return event
