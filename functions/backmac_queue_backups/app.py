#!/usr/bin/env python3

from __future__ import annotations

import json
import logging
import os
import time
import uuid

import boto3
import kubernetes as k8s
from datetime import datetime
from typing import Iterator, TYPE_CHECKING

import backmac_notify as notify
from backmac_utils import BOTO_CONFIG
from backmac_k8s_utils import build_k8s_config

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef
    from mypy_boto3_sqs.service_resource import Queue
    from mypy_boto3_sqs.type_defs import (
        BatchResultErrorEntryTypeDef,
        SendMessageBatchRequestEntryTypeDef,
        SendMessageBatchResultEntryTypeDef,
    )

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]

cfn = boto3.resource("cloudformation", config=BOTO_CONFIG)
dynamodb = boto3.resource("dynamodb", config=BOTO_CONFIG)
sqs = boto3.resource("sqs", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def chunk_messages(list_to_chunk: list, size_of_chunks: int) -> Iterator[list[SendMessageBatchRequestEntryTypeDef]]:
    """
    Yield successive chunks of a given size (size_of_chunks) from a given list (list_to_chunk).
    """
    for i in range(0, len(list_to_chunk), size_of_chunks):
        yield list_to_chunk[i : i + size_of_chunks]  # noqa E203


def get_cfn_stacks_to_backup() -> list[BackmacServiceTypeDef]:
    backup_stack_list: list[BackmacServiceTypeDef] = []
    for stack in list(cfn.stacks.all()):
        if {"Key": "backmac_enabled", "Value": "true"} in stack.tags:
            logger.info(f"Found CloudFormation stack for backup: {stack.name}")
            backup_stack_list.append({"name": stack.name, "iac": "cfn"})
    return backup_stack_list


def get_k8s_statefulsets_to_backup() -> list[BackmacServiceTypeDef]:
    backup_statefulset_list: list[BackmacServiceTypeDef] = []
    cluster_name = os.getenv("eks_cluster_name")

    if cluster_name:
        logger.info(f"Checking EKS cluster {cluster_name}")
        k8s_conf = build_k8s_config(cluster_name)

        if k8s_conf:
            with k8s.client.ApiClient(k8s_conf) as api_client:
                k8s_api = k8s.client.AppsV1Api(api_client)
                stateful_sets = k8s_api.list_stateful_set_for_all_namespaces(
                    label_selector="app.kubernetes.io/name in (jira, confluence, crowd), backmac_enabled=true",
                )
                for stateful_set in stateful_sets.items:
                    logger.info(f"Found kubernetes stateful set for backup: {stateful_set.metadata.name}")
                    backup_statefulset_list.append(
                        {
                            "name": stateful_set.metadata.name,
                            "iac": "k8s",
                            "cluster": cluster_name,
                            "namespace": stateful_set.metadata.namespace,
                        }
                    )
    else:
        logger.info("No EKS cluster name found in environment")

    return backup_statefulset_list


def push_backups_to_queue(queue: Queue, backup_machine: str, dr_region: str, service_list: list[BackmacServiceTypeDef]):
    logger.info(
        "Pushing service(s) to the queue: "
        + ", ".join([f"{service['name']} ({service['iac']})" for service in service_list])  # noqa W503
    )

    message_group_id = datetime.utcnow().strftime("%Y%m%d-%H%M%S-%f")

    # pre-format the services into objects for SQS send_message
    service_list_as_messages: list[SendMessageBatchRequestEntryTypeDef] = [
        {
            # note that this is not the ID of the SQS message, but an ID used by SQS to return which messages in the
            # batch were successfully (or failed to be) added to the queue (we don't do anything with it)
            "Id": uuid.uuid4().hex,
            "MessageBody": json.dumps(service),
            "MessageAttributes": {
                "backup_machine": {"DataType": "String", "StringValue": backup_machine},
                "dr_region": {"DataType": "String", "StringValue": dr_region},
                "retry_count": {"DataType": "Number.int", "StringValue": "0"},
                "run_type": {"DataType": "String", "StringValue": "scheduled"},
                "webhook_notification_url": {
                    "DataType": "String",
                    "StringValue": os.environ["webhook_notification_url"],
                },
            },
            "MessageGroupId": message_group_id,
        }
        for service in service_list
    ]

    successful: list[SendMessageBatchResultEntryTypeDef] = []
    failed: list[BatchResultErrorEntryTypeDef] = []
    for messages in chunk_messages(service_list_as_messages, 10):
        try:
            queue_response = queue.send_messages(Entries=messages)
        except Exception as e:
            logger.exception(f"Failure sending messages to the queue: {e}")
            raise
        else:
            logger.info(f"response from queue: {queue_response}")
            if "Successful" in queue_response:
                successful.extend(queue_response["Successful"])
            if "Failed" in queue_response:
                failed.extend(queue_response["Failed"])

    return successful, failed


def lambda_handler(event, context):
    # try to pull standard inputs from the event; use environment fallbacks
    backup_machine: str = event.get("backup_machine", os.environ["backup_machine"])
    dr_region: str = event.get("dr_region", os.environ["dr_region"])
    queue_name: str = event.get("queue_name", os.environ["queue_name"])

    service_list: list[BackmacServiceTypeDef] = []
    service_list.extend(get_cfn_stacks_to_backup())
    service_list.extend(get_k8s_statefulsets_to_backup())

    backup_history = dynamodb.Table(os.environ["backup_history_table_name"])

    queue: Queue = sqs.get_queue_by_name(QueueName=queue_name)
    successful, failed = push_backups_to_queue(queue, backup_machine, dr_region, service_list)
    duplicates = 0

    for msg in successful:
        history_item = backup_history.get_item(Key={"sqs_message_id": msg["MessageId"]}, ConsistentRead=True)
        if "Item" in history_item:
            # we found the message ID in the history; SQS will consider this message a duplicate
            dupe_message_id = history_item["Item"]["sqs_message_id"]
            existing_execution_name = history_item["Item"].get("execution_name")
            logger.warn(f"Found existing message ID in history ({dupe_message_id}); SQS will not deliver this message")
            logger.info(f"Execution already exists for ID {dupe_message_id}: {existing_execution_name}")
            duplicates += 1
        else:
            # this should not be a duplicate; add message ID to the history with a short expiration time
            # queue_handler will update this item with an execution ID, SQS receipt time, and a longer expiration time
            backup_history.put_item(
                Item={
                    "sqs_message_id": msg["MessageId"],
                    # drop this record in 1h if the queue handler never updates it
                    "expiration_time": int(time.time()) + 3600,
                    "sfn_result": "PENDING",
                }
            )

    message = f"Added {len(successful) - duplicates} service(s) to the queue for backup; {len(failed)} queueing failure(s); {duplicates} duplicate(s)"
    logger.info(message)
    notify.send("info", message, event)

    for msg in failed:
        logger.error(f"Failure queueing service: {msg}")

    return
