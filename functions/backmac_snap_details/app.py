#!/usr/bin/env python3

from __future__ import annotations

from typing import Callable, TYPE_CHECKING

import boto3
import botocore

from backmac_utils import BOTO_CONFIG, to_utc_iso_str

if TYPE_CHECKING:
    from backmac_types import ServiceSnapshotTypeDef


def populate_ebs_snapshot_details(snapshot: ServiceSnapshotTypeDef) -> ServiceSnapshotTypeDef:
    print(f"Checking EBS snapshot state in {snapshot['region']} for {snapshot['id']}")
    # use a region-specific client since we might be retrieving a snapshot in the dr_region
    ec2 = boto3.client("ec2", region_name=snapshot["region"], config=BOTO_CONFIG)
    try:
        ebs_snapshot = ec2.describe_snapshots(SnapshotIds=[snapshot["id"]])["Snapshots"][0]
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response["Error"]["Code"] == "Throttling":
            print("describe_snapshots was throttled; returning to waiter")
            return "waiting", "unknown"
        raise
    return {
        "snapshot_time": to_utc_iso_str(ebs_snapshot["StartTime"]),
        "expiry_time": to_utc_iso_str(
            next((tag["Value"] for tag in ebs_snapshot["Tags"] if tag["Key"] == "backup_delete_after"), "")
        ),
    }


def populate_rds_snapshot_details(snapshot: ServiceSnapshotTypeDef) -> ServiceSnapshotTypeDef:
    print(f"Checking RDS snapshot state in {snapshot['region']} for {snapshot['id']}")
    # use a region-specific client since we might be retrieving a snapshot in the dr_region
    rds = boto3.client("rds", region_name=snapshot["region"], config=BOTO_CONFIG)
    try:
        rds_snapshot = rds.describe_db_snapshots(DBSnapshotIdentifier=snapshot["id"])["DBSnapshots"][0]
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response["Error"]["Code"] == "Throttling":
            print("describe_db_snapshots was throttled; returning to waiter")
            return "waiting", "unknown"
        raise
    return {
        "snapshot_time": to_utc_iso_str(rds_snapshot["OriginalSnapshotCreateTime"]),
        "created_time": to_utc_iso_str(rds_snapshot["SnapshotCreateTime"]),
        "expiry_time": to_utc_iso_str(
            next((tag["Value"] for tag in rds_snapshot["TagList"] if tag["Key"] == "backup_delete_after"), "")
        ),
    }


def lambda_handler(snapshot: ServiceSnapshotTypeDef, context):
    func_name: dict[str, Callable] = {
        "ebs": populate_ebs_snapshot_details,
        "rds": populate_rds_snapshot_details,
    }

    return snapshot | func_name[snapshot["type"]](snapshot)
