#!/usr/bin/env python3

from __future__ import annotations

import hashlib
import logging

import boto3

from crhelper import CfnResource

logger = logging.getLogger(__name__)
helper = CfnResource(json_logging=True, log_level="INFO")


def md5(filename: str) -> str:
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as f:
        while chunk := f.read(4096):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


@helper.create
@helper.update
def push_parallel_sync_to_s3(event: dict, _) -> str:
    logger.info(f"Received event {event['RequestType']} for stack: {event['StackId']}")

    bucket_name: str = event["ResourceProperties"]["BucketName"]
    prefix: str = event["ResourceProperties"]["Prefix"]

    s3 = boto3.resource("s3")
    bucket = s3.Bucket(bucket_name)
    file_md5 = md5("parallel_sync")

    # prefix might be an empty string
    s3_key = "/".join(list(filter(None, [prefix, "scripts", "parallel_sync"])))

    logger.info(f"Pushing parallel_sync (md5 = {file_md5}) to s3://{bucket_name}/{s3_key}")
    bucket.upload_file(Filename="./parallel_sync", Key=s3_key)
    logger.info("Upload complete")

    return file_md5


@helper.delete
def no_op(event: dict, _) -> None:
    logger.info(f"Received event {event['RequestType']} for stack: {event['StackId']}")
    logger.info("Nothing to do...")


def lambda_handler(event, context):
    helper(event, context)
