#!/usr/bin/env python3

from __future__ import annotations

import logging

import boto3

from crhelper import CfnResource

logger = logging.getLogger(__name__)
helper = CfnResource(json_logging=True, log_level="INFO")
cfn = boto3.client("cloudformation")
cwlogs = boto3.client("logs")


def list_functions_for_stack(stack_id: str) -> list[str]:
    stack_function_names = []
    try:
        paginator = cfn.get_paginator("list_stack_resources")
        page_iterator = paginator.paginate(StackName=stack_id)
        for page in page_iterator:
            for resource in page["StackResourceSummaries"]:
                if resource["ResourceType"] == "AWS::Lambda::Function":
                    stack_function_names.append(resource["PhysicalResourceId"])
    except Exception as e:
        logger.warn(f"Unable to retrieve Lambda functions for stack {stack_id}: {e}")
    logger.info(f"Found functions: {', '.join(stack_function_names)}")
    return stack_function_names


@helper.create
@helper.update
def set_retention_for_lambda_log_groups(event: dict, _):
    stack_id: str = event["ResourceProperties"]["StackId"]
    retention_in_days: int = int(event["ResourceProperties"]["RetentionInDays"])

    for function_name in list_functions_for_stack(stack_id):
        try:
            cwlogs.put_retention_policy(
                logGroupName=f"/aws/lambda/{function_name}",
                retentionInDays=retention_in_days,
            )
        except Exception as e:
            logger.warn(f"Unable to set retention for log group /aws/lambda/{function_name}: {e}")

    return


@helper.delete
def no_op(event: dict, _) -> None:
    logger.info(f"Received event {event['RequestType']} for stack: {event['StackId']}")
    logger.info("Nothing to do...")


def lambda_handler(event, context):
    helper(event, context)
