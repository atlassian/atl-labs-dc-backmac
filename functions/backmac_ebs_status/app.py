#!/usr/bin/env python3

from __future__ import annotations

from typing import Optional, TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, get_backup_volume

if TYPE_CHECKING:
    from mypy_boto3_ec2.literals import VolumeModificationStateType
    from mypy_boto3_ec2.service_resource import Volume

ec2 = boto3.resource("ec2", config=BOTO_CONFIG)


def ebs_modification_state(backup_volume: Volume) -> Optional[VolumeModificationStateType]:
    print(f"Checking volume modification state for: {backup_volume.id}")
    try:
        ebs_modification_status: Optional[VolumeModificationStateType] = ec2.meta.client.describe_volumes_modifications(
            VolumeIds=[backup_volume.id]
        )["VolumesModifications"][0]["ModificationState"]
    except Exception:
        ebs_modification_status = None
    return ebs_modification_status


def lambda_handler(event, context):
    backup_volume = (
        ec2.Volume(event["ebs_backup_vol"])
        if "ebs_backup_vol" in event
        else get_backup_volume(event["service"]["name"])
    )

    if backup_volume.state in ["error", "deleted", "deleting"]:
        event["ebsvolume_status"] = "Failed"
    elif backup_volume.state in ["available", "in-use"]:
        # if the volume is in-use, it's probably in use by the backmac node, so just try to proceed
        modification_status = ebs_modification_state(backup_volume)
        if modification_status in ["completed", "optimizing", None]:
            event["ebsvolume_status"] = "Available"
        else:
            event["ebsvolume_status"] = modification_status
    else:
        # else use the waiter (status is most likely 'creating')
        event["ebsvolume_status"] = backup_volume.state
    return event
