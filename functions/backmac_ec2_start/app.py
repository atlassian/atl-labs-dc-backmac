#!/usr/bin/env python3

from __future__ import annotations

import logging
import os

from typing import Sequence, TYPE_CHECKING, Union

import boto3
import botocore

from backmac_exceptions import BackmacFleetRequestFailure
from backmac_utils import BOTO_CONFIG, labels_to_tags, tags_to_labels

if TYPE_CHECKING:
    from mypy_boto3_cloudformation.type_defs import TagTypeDef as CfnTagTypeDef
    from mypy_boto3_ec2.type_defs import (
        FleetLaunchTemplateConfigRequestTypeDef,
        InstanceRequirementsRequestTypeDef,
        MemoryMiBRequestTypeDef,
        OnDemandOptionsRequestTypeDef,
        SpotOptionsRequestTypeDef,
        TagSpecificationTypeDef,
        TagTypeDef as Ec2TagTypeDef,
        TargetCapacitySpecificationRequestTypeDef,
        VCpuCountRangeRequestTypeDef,
    )
    from mypy_boto3_ec2.literals import ResourceTypeType

    TagTypeDef = Union[Ec2TagTypeDef, CfnTagTypeDef]

CFN = boto3.client("cloudformation", config=BOTO_CONFIG)
EC2 = boto3.client("ec2", config=BOTO_CONFIG)
RGTA = boto3.client("resourcegroupstaggingapi", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)

INSTANCE_SIZE_TO_REQUIREMENTS: dict[str, dict[str, MemoryMiBRequestTypeDef | VCpuCountRangeRequestTypeDef]] = {
    "1x": {"MemoryMiB": {"Min": 8192, "Max": 16384}, "VCpuCount": {"Min": 4, "Max": 4}},  # xlarge
    "2x": {"MemoryMiB": {"Min": 16384, "Max": 32768}, "VCpuCount": {"Min": 8, "Max": 8}},  # 2xlarge
    "4x": {"MemoryMiB": {"Min": 32768, "Max": 65536}, "VCpuCount": {"Min": 16, "Max": 16}},  # 4xlarge
    "8x": {"MemoryMiB": {"Min": 61440, "Max": 131072}, "VCpuCount": {"Min": 32, "Max": 36}},  # 8/9xlarge
}


def get_backmac_stack_tags() -> list[TagTypeDef]:
    backmac_stack = CFN.describe_stacks(StackName=os.environ["backmac_stack_name"])
    return [tag for tag in backmac_stack["Stacks"][0]["Tags"] if tag["Key"] not in ("Name", "service_name")]


def derive_service_ownership_labels(service_name: str) -> dict[str, str]:
    """
    Search through a few known stack-owned resource types that all stacks should
    have at least one of; filter the resources by those with the tags we're
    looking for (and for the specific service we're backing up), and return the
    specific key/value pairs for the first resource that has all of them.
    """
    ownership_tag_keys = {"business_unit", "resource_owner"}
    paginator = RGTA.get_paginator("get_resources")
    response_iterator = paginator.paginate(
        ResourceTypeFilters=[
            "ec2:instance",
            "ec2:security-group",
            "elasticfilesystem:file-system",
            "rds:db",
        ],
        TagFilters=[{"Key": key} for key in ownership_tag_keys] + [{"Key": "service_name", "Values": [service_name]}],
    )
    for page in response_iterator:
        for resource in page["ResourceTagMappingList"]:
            resource_labels = tags_to_labels(resource["Tags"])
            if ownership_tag_keys <= resource_labels.keys():
                return {k: resource_labels[k] for k in ownership_tag_keys}
    return {}


def launch_fleet(event: dict, sfn_context: dict):
    # the launchtemplate will tag the instance by default with Name and service_name tags
    # build a set of labels and tag the instance and attached resources with:
    #  * the backmac run details
    #  * selected tags from the backmac stack itself
    #  * ownership tags derived from service resources
    #  * hardcoded created_by and service_name tags
    # ... in this order to ensure correct hierarchy of tags if there are duplicates
    labels: dict[str, str] = {
        "backmac_run_id": sfn_context["Execution"]["Name"],
        "backmac_run_service_name": event["service"]["name"],
        "backmac_run_service_iac": event["service"]["iac"],
    }
    labels = labels | tags_to_labels(get_backmac_stack_tags())
    labels = labels | derive_service_ownership_labels(event["service"]["name"])
    labels = labels | {"created_by": "backmac", "service_name": event["service"]["name"]}
    logger.info(f"Labels for instance: {labels}")
    resource_types: list[ResourceTypeType] = [
        "fleet",
        "instance",
        "volume",
        "network-interface",
        "spot-fleet-request",
        "spot-instances-request",
    ]
    tags_specs: Sequence[TagSpecificationTypeDef] = [
        {"ResourceType": resource_type, "Tags": labels_to_tags(labels)}  # type: ignore
        for resource_type in resource_types
    ]

    # defines the critera for the EC2 instances we want; CpuManufacturers will be
    # added in the overrides for the architecture-specific launch templates below
    # today this selects variants in the 2xlarge size by default of the following:
    # c5, c5n, c6a, c6g, c6gn, c6i, c6in, c7g, m5, m5n, m6a, m6g, m6i, m6in, m7g
    fleet_instance_requirements_base: InstanceRequirementsRequestTypeDef = {
        "BareMetal": "excluded",
        "BurstablePerformance": "excluded",
        "ExcludedInstanceTypes": [
            "a1*",
            "g5*",
            "inf*",
            "u*",
            "vt*",
            "c4*",
            "m4*",
            "c5a*",
            "m5a*",
            "m5zn*",
        ],
        "InstanceGenerations": ["current"],
        "LocalStorage": "excluded",
        "MemoryMiB": INSTANCE_SIZE_TO_REQUIREMENTS[event.get("instance_size", "2x")]["MemoryMiB"],
        "VCpuCount": INSTANCE_SIZE_TO_REQUIREMENTS[event.get("instance_size", "2x")]["VCpuCount"],
    }

    # define the options for spot and on-demand instances here just to declutter the try/except below
    fleet_spot_options: SpotOptionsRequestTypeDef = {
        "AllocationStrategy": "price-capacity-optimized",
        "InstanceInterruptionBehavior": "terminate",
        "SingleAvailabilityZone": True,
        "SingleInstanceType": False,
    }
    fleet_on_demand_options: OnDemandOptionsRequestTypeDef = {
        "AllocationStrategy": "lowest-price",
        "SingleAvailabilityZone": True,
        "SingleInstanceType": False,
    }
    fleet_target_capacity_spot_specs: TargetCapacitySpecificationRequestTypeDef = {
        "TotalTargetCapacity": 1,
        "SpotTargetCapacity": 1,
        "DefaultTargetCapacityType": "spot",
    }
    fleet_target_capacity_on_demand_specs: TargetCapacitySpecificationRequestTypeDef = {
        "TotalTargetCapacity": 1,
        "OnDemandTargetCapacity": 1,
        "DefaultTargetCapacityType": "on-demand",
    }

    # define the launch template overrides to use the correct architecture for instance selection
    fleet_launch_template_configs: list[FleetLaunchTemplateConfigRequestTypeDef] = [
        {
            "LaunchTemplateSpecification": {
                "LaunchTemplateId": os.environ["launch_template_id_x86"],
                "Version": "$Latest",
            },
            "Overrides": [
                {"InstanceRequirements": fleet_instance_requirements_base | {"CpuManufacturers": ["amd", "intel"]}}  # type: ignore
            ],
        },
        {
            "LaunchTemplateSpecification": {
                "LaunchTemplateId": os.environ["launch_template_id_arm64"],
                "Version": "$Latest",
            },
            "Overrides": [
                {
                    "InstanceRequirements": fleet_instance_requirements_base  # type: ignore
                    | {"CpuManufacturers": ["amazon-web-services"]}
                }
            ],
        },
    ]

    # by default, request an EC2 fleet of spot instance (just one in this case); failover to an on-demand instance if no spot
    # instances are available (which should be rare, given the wider range of instance classes we're targeting); even though
    # create_fleet supports requesting a mix of spot and on-demand, it doesn't automatically fulfill unavailable spot requests
    # with an on-demand request as a fallback; create an on-demand instance if instance_capacity_type requests it specifically
    instance_capacity_type = event.get("instance_capacity_type", "spot")
    match instance_capacity_type:
        case "spot":
            try:
                fleet = EC2.create_fleet(
                    LaunchTemplateConfigs=fleet_launch_template_configs,
                    SpotOptions=fleet_spot_options,
                    TagSpecifications=tags_specs,
                    TargetCapacitySpecification=fleet_target_capacity_spot_specs,
                    Type="instant",
                )
            except botocore.exceptions.ClientError as boto_err:
                # it is not clear that the EC2 client raises any exceptions for a lack of spot capacity when requesting a fleet
                # try to catch it anyways and create an on-demand fleet
                if "no Spot capacity" in boto_err.response["Error"]["Message"]:
                    fleet = EC2.create_fleet(
                        LaunchTemplateConfigs=fleet_launch_template_configs,
                        OnDemandOptions=fleet_on_demand_options,
                        TagSpecifications=tags_specs,
                        TargetCapacitySpecification=fleet_target_capacity_on_demand_specs,
                        Type="instant",
                    )
                else:
                    raise BackmacFleetRequestFailure(
                        "Failed creating spot fleet; error was not related to capacity",
                        errors=boto_err.response["Error"],
                    )
        case "on-demand":
            fleet = EC2.create_fleet(
                LaunchTemplateConfigs=fleet_launch_template_configs,
                OnDemandOptions=fleet_on_demand_options,
                TagSpecifications=tags_specs,
                TargetCapacitySpecification=fleet_target_capacity_on_demand_specs,
                Type="instant",
            )
        case _:
            raise Exception(f"Unsupported instance_capacity_type: {instance_capacity_type}")

    # if we get to this point and there are no instances, no botocore exception, but errors in the fleet request, and if
    # those errors are all related to a lack of spot capacity, try once more to create an on-demand instance
    if not fleet["Instances"] and fleet["Errors"]:
        if all(err["Lifecycle"] == "spot" and err["ErrorCode"] == "UnfulfillableCapacity" for err in fleet["Errors"]):
            fleet = EC2.create_fleet(
                LaunchTemplateConfigs=fleet_launch_template_configs,
                OnDemandOptions=fleet_on_demand_options,
                TagSpecifications=tags_specs,
                TargetCapacitySpecification=fleet_target_capacity_on_demand_specs,
                Type="instant",
            )
        else:
            raise BackmacFleetRequestFailure(
                "Unable to create spot or on-demand fleet/instance with instance requirements", errors=fleet["Errors"]
            )
    logger.info(f"Fleet details: {fleet}")
    return fleet


def lambda_handler(params, context):
    event = params["event"]
    sfn_context = params["sfn_context"]

    try:
        backmac_fleet = launch_fleet(event, sfn_context)
    except BackmacFleetRequestFailure as e:
        logger.exception(f"Errors creating backmac fleet: {e.errors}")
        raise

    event["backmac_fleet_id"] = backmac_fleet["FleetId"]
    event["backmac_instance"] = backmac_fleet["Instances"][0]["InstanceIds"][0]
    return event
