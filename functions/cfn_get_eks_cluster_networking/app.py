#!/usr/bin/env python3

from __future__ import annotations

import logging
import os

import boto3
import botocore

from crhelper import CfnResource

logger = logging.getLogger(__name__)
helper = CfnResource(json_logging=True, log_level="INFO")


@helper.create
@helper.update
def get_eks_cluster_networking(event: dict, _) -> str:
    logger.info(f"Received event {event['RequestType']} for stack: {event['StackId']}")

    cluster_name: str = event["ResourceProperties"]["ClusterName"]
    boto_config = botocore.config.Config(retries={"max_attempts": 10}, region_name=os.environ["AWS_REGION"])
    eks = boto3.client("eks", config=boto_config)

    logger.info(f"Getting EKS cluster networking details for cluster {cluster_name}")

    try:
        cluster_details = eks.describe_cluster(name=cluster_name)["cluster"]
    except eks.exceptions.ResourceNotFoundException as e:
        raise Exception(f"EKS cluster {cluster_name} not found") from e

    eks_cluster_security_group = cluster_details["resourcesVpcConfig"]["clusterSecurityGroupId"]
    eks_cluster_subnets = cluster_details["resourcesVpcConfig"]["subnetIds"]

    logger.info(f"Found security group(s): {eks_cluster_security_group}")
    logger.info(f'Found subnets: {", ".join(eks_cluster_subnets)}')

    helper.Data.update(
        {"EKSClusterSecurityGroups": [eks_cluster_security_group], "EKSClusterSubnets": eks_cluster_subnets}
    )

    return f"{eks_cluster_security_group}:{','.join(eks_cluster_subnets)}"


@helper.delete
def no_op(event: dict, _) -> None:
    logger.info(f"Received event {event['RequestType']} for stack: {event['StackId']}")
    logger.info("Nothing to do...")


def lambda_handler(event, context):
    helper(event, context)
