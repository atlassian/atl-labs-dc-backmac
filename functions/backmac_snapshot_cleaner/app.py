#!/usr/bin/env python3

"""
Searches snapshots containing the 'backup_date', 'backup_delete_after' tag-keys and
removes all snapshots that have expired as per the 'backup_delete_after' tag
"""

import logging
import os
from datetime import datetime, timedelta, timezone
from dateutil import parser

import boto3
import botocore

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# we're not importing the shared/global BOTO_CONFIG from backmac_utils for now
# as snapshot deletion tends to require more throughput of API calls than normal
# backmac runs (where some retry is needed, but past a point failure is preferred,
# whereas it doesn't matter if this lambda runs for the entire execution limit)
BOTO_CONFIG = botocore.config.Config(
    retries={"max_attempts": 15, "mode": "adaptive"},
    region_name=os.environ["AWS_REGION"],
)
EC2 = boto3.client("ec2", config=BOTO_CONFIG)
RDS = boto3.client("rds", config=BOTO_CONFIG)


def clean_ebs_snapshots():
    # TODO: paginate through these snapshots instead of retrieiving them all at once (allow for smaller memory footprint)
    logger.info(f'Retrieving EBS snapshots in region: {os.environ["AWS_REGION"]}')

    # get a list of all snapshots owned by us with the backup_date, backup_delete_after tags
    snapshots = EC2.describe_snapshots(
        Filters=[
            {"Name": "tag-key", "Values": ["backup_date"]},
            {"Name": "tag-key", "Values": ["backup_delete_after"]},
        ],
        OwnerIds=["self"],
    )

    # loop over snapshots and blow away anything passed its used by date
    logger.info(f'Cleaning EBS snapshots in {os.environ["AWS_REGION"]}')
    for snapshot in snapshots["Snapshots"]:
        for tag in snapshot["Tags"]:
            if tag["Key"] == "Name":
                name = tag["Value"]
            if tag["Key"] == "backup_delete_after":
                backup_delete_after = tag["Value"]
                # convert backup_delete_after string to a datetime object
                backup_delete_after = parser.parse(backup_delete_after)
            if tag["Key"] == "backup_date":
                backup_date = tag["Value"]

        if datetime.now() > backup_delete_after:
            logger.info(
                f'Removing EBS SnapshotId: {snapshot["SnapshotId"]}, name: {name}, backup_date: {backup_date}, backup_delete_after: {backup_delete_after}'
            )
            try:
                EC2.delete_snapshot(SnapshotId=snapshot["SnapshotId"])
            except botocore.exceptions.ClientError as boto_err:
                if boto_err.response["Error"]["Code"] == "UnauthorizedOperation":
                    logger.error(f'Unauthorized to delete EBS snapshot: {name} ({snapshot["SnapshotId"]})')
                elif boto_err.response["Error"]["Code"] == "InvalidSnapshot.NotFound":
                    logger.warn(f'Missing (not found) EBS snapshot: {name} ({snapshot["SnapshotId"]})')
                else:
                    raise


def clean_rds_snapshots(marker):
    logger.info(f'Retrieving RDS snapshots in region: {os.environ["AWS_REGION"]}')
    # get a list of all snapshots owned by us with the backup_date, backup_delete_after tags
    snapshots = RDS.describe_db_snapshots(
        Marker=marker,
        SnapshotType="manual",
        # tag-key filters not supported on describe_db_snapshots :(
        # Filters=[
        #    {'Name': 'tag-key', 'Values': ['backup_date']},
        #    {'Name': 'tag-key', 'Values': ['backup_delete_after']},
        # ],
    )

    # boto RDS client isnt as mature as the EC2 client, tags are not available from boto3.client.describe_db_snapshots()
    # loop over snapshots, scrape arn, scrape tags from arn to determine if we can remove the snapshot
    # this is significantly more expensive than the EC2 cleanup
    logger.info(f'Cleaning RDS snapshots in {os.environ["AWS_REGION"]}')
    for snapshot in snapshots["DBSnapshots"]:
        tags = RDS.list_tags_for_resource(ResourceName=snapshot["DBSnapshotArn"])
        backup_date = "NOT FOUND"
        backup_delete_after = "NOT FOUND"
        if len(tags["TagList"]) > 0:
            # snapshot has tags
            for tag in tags["TagList"]:
                if "backup_date" in tag.values():
                    backup_date = tag["Value"]
                if "backup_delete_after" in tag.values():
                    backup_delete_after = tag["Value"]
            if "NOT FOUND" not in backup_delete_after:
                backup_delete_after = parser.parse(backup_delete_after)
                if datetime.now() > backup_delete_after:
                    logger.info(
                        f'Removing RDS SnapshotId: {snapshot["DBSnapshotIdentifier"]}, backup_date: {backup_date}, backup_delete_after: {backup_delete_after}'
                    )
                    delete_rds_snapshot(snapshot["DBSnapshotIdentifier"])
        else:
            # there are no tags, flag it if it's older than 30 days.
            backup_delete_after = snapshot["SnapshotCreateTime"] + timedelta(days=30)
            snapshot_age = datetime.now(timezone.utc) - snapshot["SnapshotCreateTime"]
            if datetime.now(timezone.utc) > backup_delete_after:
                logger.info(
                    f'Found an old untagged RDS Snapshot: SnapshotId: {snapshot["DBSnapshotIdentifier"]}, (no tags), backup_delete_after: {backup_delete_after} : {snapshot_age.days} days old'
                )
                # if you want to delete your old snapshots, uncomment the following line.
                # delete_rds_snapshot(snapshot['DBSnapshotIdentifier'])

    if snapshots.get("Marker") is None:
        return None
    else:
        return snapshots["Marker"]


def delete_rds_snapshot(dbsnapshotid):
    try:
        RDS.delete_db_snapshot(DBSnapshotIdentifier=dbsnapshotid)
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response["Error"]["Code"] == "AccessDenied":
            logger.error(f"Unauthorized to delete RDS snapshot: {dbsnapshotid}")
        else:
            raise


def lambda_handler(event, context):
    """
    main entry point
    """
    logger.info("Starting handler")
    logger.info("Loading lambda...\n---------------------")
    clean_ebs_snapshots()

    marker = ""
    while marker is not None:
        marker = clean_rds_snapshots(marker)
