#!/usr/bin/env python3

import json

from typing import TYPE_CHECKING

import backmac_notify as notify

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef


def lambda_handler(params, context):  # noqa: C901
    event = params["event"]
    sfn_context = params["sfn_context"]

    # look for the error at the root of the event or on a $.error ResultPath
    try:
        error = event["Error"]
    except KeyError:
        try:
            error = event["error"]["Error"]
        except KeyError:
            error = "UnknownError"

    # try to parse the error as json; swallow any failures
    try:
        error = json.loads(error)
    except json.decoder.JSONDecodeError:
        pass

    # look for the cause at the root of the event or on a $.error ResultPath
    try:
        cause = event["Cause"]
    except KeyError:
        try:
            cause = event["error"]["Cause"]
        except KeyError:
            cause = {
                "errorMessage": "Unknown error; check SFN logs",
                "errorType": "UnknownError",
            }

    # try to parse the cause as json; swallow any failures
    try:
        cause = json.loads(cause)
    except json.decoder.JSONDecodeError:
        pass
    except TypeError:
        pass

    event["error_details"] = {"error": error, "cause": cause}

    try:
        event["error_details"]["function_name"] = event["current_function_name"]
        event["error_details"]["log_group_name"] = event["current_log_group_name"]
        event["error_details"]["log_stream_name"] = event["current_log_stream_name"]
    except KeyError:
        pass

    message_parts: list[str] = ["Iteration failed"]
    service: BackmacServiceTypeDef = event.get("service")
    if service:
        if "name" in service and "iac" in service:
            message_parts.append(f"for {service['name']} ({service['iac']})")
        elif any(key for key in service.keys() if key.endswith("_id")):
            message_parts.append("for")
            message_parts.append(", ".join([f"{k}: {v}" for k, v in service.items() if k.endswith("_id")]))

    print(notify.send("failure", " ".join(message_parts), event, sfn_context))
    return event
