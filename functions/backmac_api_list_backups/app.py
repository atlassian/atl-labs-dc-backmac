#!/usr/bin/env python3

from __future__ import annotations

import json
import logging
import os
import time

from typing import Callable

import boto3

from boto3.dynamodb.conditions import Attr, Key

from backmac_utils import BOTO_CONFIG

DYNAMODB = boto3.resource("dynamodb", config=BOTO_CONFIG)
SFN = boto3.client("stepfunctions", config=BOTO_CONFIG)

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_PARTITION = os.environ["AWS_PARTITION"]
AWS_REGION = os.environ["AWS_REGION"]

ALLOWED_STATUS_VALUES = {"FAILED", "PENDING", "RUNNING", "SUCCEEDED"}

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(request, context):
    logger.info(f"request: {request}")

    response_obj: dict = {"backups": []}

    backup_history = DYNAMODB.Table(os.environ["backup_history_table_name"])

    # set defaults
    service_name: str = ""
    status: str = ""
    max_age = 86400

    # override defaults with data from query string
    if request["queryStringParameters"]:
        service_name = request["queryStringParameters"].get("service_name", "")
        status = request["queryStringParameters"].get("status", "")
        max_age = int(request["queryStringParameters"].get("max_age", max_age))
        if status and status not in ALLOWED_STATUS_VALUES:
            return {
                "statusCode": 400,
                "body": json.dumps(
                    {
                        "error_msg": f"Unexpected value '{status}' for request parameter 'status': oneOf[{(', ').join(ALLOWED_STATUS_VALUES)}]"
                    }
                ),
            }

    # set timestamp with max_age value
    max_age_tstamp = int(time.time()) - max_age

    # set our primary method of querying the backup history table
    query_method: Callable = backup_history.query

    # if service and status, query by the service name index and filter by the status attribute
    if service_name and status:
        query_params = {
            "IndexName": "service_name-sqs_receive_time-idx",
            "KeyConditionExpression": Key("service_name").eq(request["queryStringParameters"]["service_name"])
            & Key("sqs_receive_time").gte(max_age_tstamp),
            "FilterExpression": Attr("sfn_result").eq(request["queryStringParameters"]["status"]),
        }
    # if only status, query by the status index
    elif status:
        query_params = {
            "IndexName": "sfn_result-sqs_receive_time-idx",
            "KeyConditionExpression": Key("sfn_result").eq(request["queryStringParameters"]["status"])
            & Key("sqs_receive_time").gte(max_age_tstamp),
        }
    # if only service, query by the service name index
    elif service_name:
        query_params = {
            "IndexName": "service_name-sqs_receive_time-idx",
            "KeyConditionExpression": Key("service_name").eq(request["queryStringParameters"]["service_name"])
            & Key("sqs_receive_time").gte(max_age_tstamp),
        }
    else:
        # if no service_name or status, scan the entire table
        query_method = backup_history.scan
        query_params = {
            "ConsistentRead": True,
            "FilterExpression": Key("sqs_receive_time").gte(max_age_tstamp),
        }

    try:
        # TODO: allow callers to specify length of time?
        query_results = query_method(**query_params)
    except Exception as e:
        logger.exception("Failed to retrieve backup history from DynamoDB")
        return {
            "statusCode": 500,
            "body": json.dumps({"error_msg": f"Unhandled exception: {getattr(e, 'message', repr(e))}"}),
        }

    backups = [
        {
            "id": item.get("sqs_message_id"),
            "name": item.get("execution_name"),
            "status": item.get("sfn_result"),
        }
        for item in query_results["Items"]
    ]

    response_obj["backups"] = sorted(backups, key=lambda backup: backup["name"], reverse=True)

    return {"statusCode": 200, "body": json.dumps(response_obj)}
