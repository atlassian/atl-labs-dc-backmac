#!/usr/bin/env python3

from __future__ import annotations

import json

import boto3

from backmac_exceptions import SsmExecutionError
from backmac_utils import BOTO_CONFIG, ssm_wait_response
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance
    from mypy_boto3_ec2.literals import InstanceStateNameType, SummaryStatusType

EC2 = boto3.resource("ec2", config=BOTO_CONFIG)


def ec2_state(backmac_instance: Instance) -> tuple[InstanceStateNameType, SummaryStatusType, SummaryStatusType]:
    print(f"Check instance state for: {backmac_instance.id}")
    ec2_instance_status = EC2.meta.client.describe_instance_status(
        IncludeAllInstances=True, InstanceIds=[backmac_instance.id]
    )["InstanceStatuses"][0]
    return (
        ec2_instance_status["InstanceState"]["Name"],
        ec2_instance_status["InstanceStatus"]["Status"],
        ec2_instance_status["SystemStatus"]["Status"],
    )


def cloud_init_successful(backmac_instance: Instance) -> bool:
    try:
        cloud_init_result = json.loads(ssm_wait_response(backmac_instance, "cat /run/cloud-init/result.json"))
    except (SsmExecutionError, ValueError) as e:
        print(e)
        return False
    # TODO: remove second phase of conditional; added as workaround for AWS Support case 6795199311
    if cloud_init_result["v1"]["errors"]:
        if not (
            len(cloud_init_result["v1"]["errors"]) == 1
            and "package-update-upgrade-install" in cloud_init_result["v1"]["errors"][0]
        ):
            raise Exception("Encountered errors in cloud-init: ", cloud_init_result["v1"]["errors"])
    return True


def lambda_handler(event, context):
    try:
        backmac_instance = EC2.Instance(event["backmac_instance"])
    except Exception:
        raise Exception("No backmac_instance found in event")

    try:
        ec2_status, instance_status, system_status = ec2_state(backmac_instance)
    except IndexError:
        print(
            f"status for instance {backmac_instance.id} is empty; instance may still be starting (i.e., not even pending)"
        )
        ec2_status = "spooling"

    if ec2_status in {"shutting-down", "terminated", "stopping", "stopped"}:
        # fail now if the instance is in a terminal state
        raise Exception(
            f'EC2 instance {backmac_instance.id} is "{ec2_status}" due to "{backmac_instance.state_reason["Message"]}"'
        )
    elif ec2_status == "pending":
        event["ec2_status"] = "pending"
    elif ec2_status == "running":
        if instance_status == "ok" and system_status == "ok":
            if "ec2_status" in event and event["ec2_status"] == "ready":
                # if we're at this point, we really just want to know if the instance has been terminated during EFS sync, so if the
                # instance is 'running' with good status checks and the event already knows the instance is 'ready', we've been here
                # before and can skip the cloud_init check
                pass
            else:
                event["ec2_status"] = "ready" if cloud_init_successful(backmac_instance) else "pending"
                event["ec2_class"] = backmac_instance.instance_type
        elif instance_status == "impaired" or system_status == "impaired":
            raise Exception(
                f'EC2 instance {backmac_instance.id} has instance status "{instance_status}" and system status "{system_status}"'
            )
        else:
            event["ec2_status"] = "pending"
    elif ec2_status == "spooling" and "ec2_status" in event and event["ec2_status"] == "spooling":
        # we've been through the status/waiter loop once already and the instance is still spooling? consider this a failure
        raise Exception(
            f'EC2 instance {backmac_instance.id} failed to reach "pending" status after 2 consecutive status checks'
        )
    else:
        raise Exception(f"Unexpected response from ec2_state: {ec2_status}")

    return event
