#!/usr/bin/env python3

from __future__ import annotations

import logging

from os import getenv

import boto3

from backmac_utils import BOTO_CONFIG, ssm_wait_response
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ec2.service_resource import Instance, Volume
    from mypy_boto3_ec2.type_defs import ModifyVolumeResultTypeDef

EC2 = boto3.resource("ec2", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def resize_backup_volume(backmac_instance: Instance, backup_volume: Volume) -> ModifyVolumeResultTypeDef:
    # convert byte to gb for index snapshotsize
    try:
        indexsnapshot_size = float(get_indexsnapshot_size(backmac_instance)) / 1073741824
    except ValueError:
        # a value was returned, but we couldn't convert it to a float; likely there are no snapshots
        # this will always be the case for bitbucket and crowd
        logger.exception("Index snapshot size could not be cast to float; stack may not have index snapshots!")
        indexsnapshot_size = 0

    # The trigger for a resize is if difference between ebs and efs size is less than 15 percent,
    # however increasing in small increments should be enough and in practice another small resize can be re-triggered on next run.
    # Add 5% to volumesize. This allows larger volumes to grow by a reasonble size.
    # gp2/gp3 has constraints of 1-16384, so this might still fail if volumesize * 1.05 < 0.5GiB , so adding +5
    new_size = int(round((backup_volume.size * 1.05) + (indexsnapshot_size * 3) + 5))

    logger.info(f"resizing volume {backup_volume.id} from {backup_volume.size} to {new_size}")
    response = EC2.meta.client.modify_volume(
        VolumeId=backup_volume.id, Size=new_size, VolumeType=getenv("backmac_node_volume_type", "gp2")
    )  # type: ignore
    return response


def get_indexsnapshot_size(backmac_instance: Instance):
    total_index_space = ssm_wait_response(
        backmac_instance,
        "ls -lSr /media/atl/*/sh*/{export/indexsnapshots,index-snapshots,caches/indexesV2}/Index* 2>/dev/null | tail -1 | cut -d' ' -f5",
    )
    return total_index_space


def lambda_handler(event, context):
    backmac_instance = EC2.Instance(event["backmac_instance"])
    backup_volume = EC2.Volume(event["ebs_backup_vol"])

    response = resize_backup_volume(backmac_instance, backup_volume)
    logger.info(response)

    return event
