#!/usr/bin/env python3

from __future__ import annotations

import os

from typing import Callable, TYPE_CHECKING

import boto3

from backmac_utils import BOTO_CONFIG, clean_labels, labels_to_tags, tags_to_labels

if TYPE_CHECKING:
    from backmac_types import ServiceSnapshotTypeDef

rds = boto3.client("rds", config=BOTO_CONFIG)
ec2 = boto3.client("ec2", config=BOTO_CONFIG)

AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_REGION = os.environ["AWS_REGION"]


def copy_ebs_snap_todr(ebs_snap: ServiceSnapshotTypeDef, dr_region: str, kms_key_arn: str) -> ServiceSnapshotTypeDef:
    # Copy the tags from the original snapshot; overwrite a few tags while doing so
    source_snap = ec2.describe_snapshots(SnapshotIds=[ebs_snap["id"]])["Snapshots"][0]
    source_snap_labels = tags_to_labels(source_snap["Tags"])
    source_snap_name = source_snap_labels["Name"]

    snap_labels: dict[str, str] = {}
    snap_labels = snap_labels | clean_labels(source_snap_labels)
    snap_labels = snap_labels | {
        "Name": f"dr_{source_snap_name}",
        "backup_lambda_log_group_name": f"{os.getenv('AWS_LAMBDA_LOG_GROUP_NAME', '')}",
        "backup_lambda_log_stream_guid": f"{os.getenv('AWS_LAMBDA_LOG_STREAM_NAME', '').split(']')[-1]}",
        "source_region": AWS_REGION,
    }
    snap_tags = labels_to_tags(snap_labels)

    copy_params = {
        "Description": f"dr {source_snap['Description']}",
        "SourceRegion": AWS_REGION,
        "SourceSnapshotId": ebs_snap["id"],
        "TagSpecifications": [{"ResourceType": "snapshot", "Tags": snap_tags}],
    }

    if kms_key_arn:
        copy_params["Encrypted"] = True
        copy_params["KmsKeyId"] = kms_key_arn

    ec2_dr = boto3.client("ec2", region_name=dr_region, config=BOTO_CONFIG)
    print(f"Copying snapshot {source_snap_name} to: {dr_region}")

    # EC2's copy_snapshot only returns the snapshot ID and any tags that were set at creation,
    # so create the snap and return the ID and a dummy status to be updated by snap_status
    dr_snap = ec2_dr.copy_snapshot(**copy_params)

    return {
        "id": dr_snap["SnapshotId"],
        "region": dr_region,
        "type": "ebs",
        "status": "waiting",
    }


def copy_rds_snap_to_dr(rds_snap: ServiceSnapshotTypeDef, dr_region: str, kms_key_arn: str) -> ServiceSnapshotTypeDef:
    # Copy the tags from the original snapshot; overwrite a few tags while doing so
    source_snap = rds.describe_db_snapshots(DBSnapshotIdentifier=rds_snap["id"])["DBSnapshots"][0]
    source_snap_labels = tags_to_labels(source_snap["TagList"])
    source_snap_name = source_snap_labels["Name"]

    snap_labels: dict[str, str] = {}
    snap_labels = snap_labels | clean_labels(source_snap_labels)
    snap_labels = snap_labels | {
        "Name": f"dr-{source_snap_name}",
        "backup_lambda_log_group_name": f"{os.getenv('AWS_LAMBDA_LOG_GROUP_NAME', '')}",
        "backup_lambda_log_stream_guid": f"{os.getenv('AWS_LAMBDA_LOG_STREAM_NAME', '').split(']')[-1]}",
        "source_region": AWS_REGION,
    }

    copy_params = {
        "SourceDBSnapshotIdentifier": f"arn:aws:rds:{AWS_REGION}:{AWS_ACCOUNT_ID}:snapshot:{rds_snap['id']}",
        "TargetDBSnapshotIdentifier": f"dr-{rds_snap['id']}",
        "Tags": labels_to_tags(snap_labels),
        "SourceRegion": rds_snap["region"],
    }

    if kms_key_arn:
        copy_params["KmsKeyId"] = kms_key_arn

    rds_dr = boto3.client("rds", config=BOTO_CONFIG, region_name=dr_region)
    print(f"Copying snapshot {rds_snap['id']} to: {dr_region}")

    dr_snap = rds_dr.copy_db_snapshot(**copy_params)["DBSnapshot"]

    return {
        "id": dr_snap["DBSnapshotIdentifier"],
        "region": dr_region,
        "type": "rds",
        "status": dr_snap["Status"],
        "db_engine": dr_snap["Engine"],
        "db_engine_version": dr_snap["EngineVersion"],
    }


def lambda_handler(iterator, context) -> ServiceSnapshotTypeDef:
    snapshot: ServiceSnapshotTypeDef = iterator["snapshot"]
    event = iterator["event"]

    # If the source snapshot is encrypted, the DR snap will be automatically, but using the default service-specific KMS
    # key provided by AWS if we don't specify our own key during the copy operation. The method used below ensures that
    # if we encounter a service using different keys for EBS vs RDS, we'll use the correct key for this snapshot
    if len(event["source_kms_key_arns"]) > 0:
        # use the KMS key that corresponds to this resource type (if there are multiple keys)
        dr_snapshot_kms_key_arn = next(
            (key for key, services in event["dr_kms_key_arns"].items() if snapshot["type"] in services), None
        )
    else:
        dr_snapshot_kms_key_arn = None

    func_name: dict[str, Callable] = {
        "ebs": copy_ebs_snap_todr,
        "rds": copy_rds_snap_to_dr,
    }

    return func_name[snapshot["type"]](snapshot, event["dr_region"], dr_snapshot_kms_key_arn)
