#!/usr/bin/env python3

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ec2.type_defs import CreateFleetErrorTypeDef


class SsmExecutionError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.return_code = kwargs.get("return_code")


class SsmInstanceError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.instance_state = kwargs.get("instance_state")
        self.state_reason = kwargs.get("state_reason")


class BackmacVolumeNotFound(Exception):
    pass


class BackmacFleetRequestFailure(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.errors: list[CreateFleetErrorTypeDef] = kwargs.get("errors")
