#!/usr/bin/env python3

from __future__ import annotations

import os
import time

from datetime import datetime
from zoneinfo import ZoneInfo
from typing import cast

import boto3
import botocore
import kubernetes as k8s

from backmac_exceptions import SsmExecutionError, SsmInstanceError, BackmacVolumeNotFound
from backmac_k8s_utils import build_k8s_config
from typing import Any, Callable, Literal, TYPE_CHECKING, Sequence, Union

if TYPE_CHECKING:
    from backmac_types import BackmacServiceTypeDef
    from mypy_boto3_cloudformation.type_defs import TagTypeDef as CfnTagTypeDef
    from mypy_boto3_ec2.service_resource import Instance, Volume
    from mypy_boto3_sqs.service_resource import Message, Queue
    from mypy_boto3_ec2.type_defs import TagTypeDef as Ec2TagTypeDef

    TagTypeDef = Union[Ec2TagTypeDef, CfnTagTypeDef]

BOTO_CONFIG = botocore.config.Config(retries={"max_attempts": 10}, region_name=os.environ["AWS_REGION"])

ec2 = boto3.resource("ec2", config=BOTO_CONFIG)
cfn = boto3.client("cloudformation", config=BOTO_CONFIG)
ssm = boto3.client("ssm", config=BOTO_CONFIG)
sqs = boto3.resource("sqs", config=BOTO_CONFIG)


def get_backup_retention(service: BackmacServiceTypeDef) -> int:
    backmac_retention = 30
    try:
        backmac_retention = int(service["labels"]["backmac_retention_days"])
    except KeyError:
        print(f"No value found for backmac_retention_days; using default ({backmac_retention})")
    except ValueError:
        print(
            f"The value found for backmac_retention_days ({backmac_retention}) is not / could not be cast to an integer"
        )
    return backmac_retention


def get_backup_volume(service_name: str) -> Volume:
    volumes = ec2.meta.client.describe_volumes(Filters=[{"Name": "tag:Name", "Values": [service_name + "-backup"]}])[
        "Volumes"
    ]

    if not volumes:
        raise BackmacVolumeNotFound(f"no backup volume found for service {service_name}")

    if len(volumes) > 1:
        raise Exception(f"more than one backup volume found! ({', '.join([vol['VolumeId'] for vol in volumes])})")

    print(f"backup volumeId is: {volumes[0]['VolumeId']}")
    return ec2.Volume(volumes[0]["VolumeId"])


# TODO: query a pod from a statefulset for its persistent storage claim; find the custom resource
# type and details (so we don't have to hardcode them in get_k8s_resource_id)
# def find_k8s_persistent_storage_type(cluster: str, name: str):
#     k8s_conf = build_k8s_config(cluster)


def get_cfn_resource_id(name: str, type: Literal["DB", "EBSRestoreVol", "ElasticFileSystem"], **_kwargs) -> str:
    stack_resource = cfn.describe_stack_resource(LogicalResourceId=type, StackName=name)
    return stack_resource["StackResourceDetail"]["PhysicalResourceId"]


def get_k8s_resource_id(cluster: str, name: str, namespace: str, type: Literal["efs", "rds"], **_kwargs) -> str:
    k8s_conf = build_k8s_config(cluster)
    # we're going to assume the namespace is the name without the final segment:
    # namespace = '-'.join(name.split('-')[0:-1])
    # the name of the object is actually slightly different and looks more like the namespace with the suffix of '-backmac':
    configmap_name = f"{namespace}-backmac"
    with k8s.client.ApiClient(k8s_conf) as api_client:
        k8s_api = k8s.client.CoreV1Api(api_client)
        resource = k8s_api.read_namespaced_config_map(configmap_name, namespace)
    return resource.data[type]


def get_service_resource_id(service: BackmacServiceTypeDef, resource_type: str) -> str:
    func_name: dict[str, Callable] = {
        "cfn": get_cfn_resource_id,
        "k8s": get_k8s_resource_id,
    }
    type_param = {
        "cfn": {"ebs": "EBSRestoreVol", "efs": "ElasticFileSystem", "rds": "DB"},
        "k8s": {"efs": "efs", "rds": "rds"},
    }
    return func_name[service["iac"]](**service, type=type_param[service["iac"]][resource_type])


def get_stack_tags(name: str, **_kwargs) -> dict[str, str]:
    try:
        cfn_tags = cfn.describe_stacks(StackName=name)["Stacks"][0]["Tags"]
    except botocore.exceptions.ClientError as e:
        raise Exception(f"Encountered boto error retrieving tags for stack {name}") from e
    return tags_to_labels(cfn_tags)


def get_statefulset_labels(cluster: str, namespace: str, name: str, **_kwargs) -> dict[str, str]:
    k8s_conf = build_k8s_config(cluster)
    with k8s.client.ApiClient(k8s_conf) as api_client:
        k8s_api = k8s.client.AppsV1Api(api_client)
        try:
            stateful_set = k8s_api.read_namespaced_stateful_set(name=name, namespace=namespace)
        except k8s.client.exceptions.ApiException as e:
            raise Exception(
                f'Encountered ApiException retrieving labels for statefulset in cluster "{cluster}" namespace "{namespace}" with name "{name}"'
            ) from e
    return stateful_set.metadata.labels


def get_service_tags_or_labels(service: BackmacServiceTypeDef) -> dict[str, str]:
    if "iac" not in service:
        return {}
    func_name: dict[str, Callable] = {
        "k8s": get_statefulset_labels,
        "cfn": get_stack_tags,
    }
    return func_name[service["iac"]](**service)


def tags_to_labels(tags: Sequence[TagTypeDef]) -> dict[str, str]:
    return {tag["Key"]: tag["Value"] for tag in tags}


def labels_to_tags(labels: dict[str, str]) -> Sequence[TagTypeDef]:
    return [{"Key": key, "Value": value} for key, value in labels.items()]


def clean_labels(tags: dict[str, str], extra_keys: list[str] = []) -> dict[str, str]:
    tag_keys_to_remove = {
        "Application",
        "backmac_enabled",
        "clustered",
        "repository",
        "template",
    }  # a default set of keys we don't care about
    tag_keys_to_remove.update(extra_keys)  # any extra keys the caller wants removed
    return {key: value for key, value in tags.items() if key not in tag_keys_to_remove and not key.startswith("aws:")}


def read_sqs_queue(queue: Queue) -> list[Message]:
    """
    Passive reader; retrieves and de-duplicates messages from the provided SQS queue and does not pop messages from the
    queue; leaves messages visible for other consumers (within 1s).
    """

    # the Message objects returned by receive_messages are unique because the receipt_handle for each instance of a
    # returned message is unique, even if the message content is the same; for this reason, we'll keep track of the
    # provided md5s of the message body and attributes as tuples in a set we can quickly compare to for de-duplication
    # of received messages
    seen_message_md5s: set[tuple[str, str]] = set()
    queue_messages: list[Message] = []
    receive_params: dict[str, Any] = {
        "MaxNumberOfMessages": 10,  # max
        "MessageAttributeNames": ["All"],
        "VisibilityTimeout": 1,  # prevents receiving the same message multiple times in a single response
        "WaitTimeSeconds": 5,
    }

    # grab the sum of ApproximateNumberOfMessages, ApproximateNumberOfMessagesNotVisible, and
    # ApproximateNumberOfMessagesDelayed to approximate how many times we should iterate through the loop
    theoretical_number_of_messages = sum(
        int(v) for k, v in queue.attributes.items() if k.startswith("ApproximateNumberOfMessages")
    )

    # don't allow the loop below to run indefinitely if for some reason we never collect the expected number of messages
    max_time = 12  # slightly more than double WaitTimeSeconds ¯\_(ツ)_/¯
    start_time = time.time()

    while len(queue_messages) < theoretical_number_of_messages:
        if (time.time() - start_time) > max_time:
            break
        messages = queue.receive_messages(**receive_params)
        for message in messages:
            msg_hashes = (message.md5_of_body, message.md5_of_message_attributes)
            if msg_hashes not in seen_message_md5s:
                queue_messages.append(message)
                seen_message_md5s.add((message.md5_of_body, message.md5_of_message_attributes))

    return queue_messages


def ssm_wait_response(backmac_instance: Instance, cmd: str, exec_timeout: int | None = None) -> str:
    command_params: dict = {
        "InstanceIds": [backmac_instance.id],
        "DocumentName": "AWS-RunShellScript",
        "Parameters": {"commands": [cmd]},
    }

    if exec_timeout:
        command_params["Parameters"]["executionTimeout"] = [str(exec_timeout)]

    try:
        command_params["OutputS3BucketName"] = os.environ["backmac_s3_bucket"]
        command_params["OutputS3KeyPrefix"] = "/".join([os.environ["backmac_s3_prefix"], "ssm_commands"])
    except KeyError:
        pass

    if backmac_instance.state["Name"] != "running":
        raise SsmInstanceError(
            f"SSM command cannot be sent; instance {backmac_instance.id} is not running!",
            instance_state=backmac_instance.state["Name"],
            state_reason=backmac_instance.state_reason["Message"],
        )

    ssm_command = ssm.send_command(**command_params)

    print(f'for command: {cmd} CommandId is: {ssm_command["Command"]["CommandId"]}')
    status = "Pending"

    while status in ("Pending", "InProgress"):
        time.sleep(3)

        backmac_instance.reload()
        if backmac_instance.state["Name"] != "running":
            raise SsmInstanceError(
                f"SSM command incomplete; instance {backmac_instance.id} is not running!",
                instance_state=backmac_instance.state["Name"],
                state_reason=backmac_instance.state_reason["Message"],
            )

        list_command = ssm.list_commands(CommandId=ssm_command["Command"]["CommandId"])
        status = list_command["Commands"][0]["Status"]
        print(f'for command: {cmd} status is: {list_command["Commands"][0]["Status"]}')

    result = ssm.get_command_invocation(
        CommandId=ssm_command["Command"]["CommandId"],
        InstanceId=backmac_instance.id,
    )

    if result["Status"] == "TimedOut":
        print(f"SSM command timed out after {result['ExecutionElapsedTime']}; exec_timeout was: {exec_timeout}")
        raise SsmExecutionError(
            f"SSM command timed out after {result['ExecutionElapsedTime']}! Error: {result['StatusDetails']}",
            return_code=result["ResponseCode"],
        )

    if result["ResponseCode"] != 0:
        raise SsmExecutionError(
            f'SSM command failed! Error: {result["StandardErrorContent"]}',
            return_code=result["ResponseCode"],
        )

    return result["StandardOutputContent"].rstrip()


def to_utc_iso_str(time: datetime | str) -> str:
    try:
        assert isinstance(time, (datetime, str))
    except AssertionError:
        return ""
    try:
        timestamp = datetime.fromisoformat(cast(str, time))
    except TypeError:
        timestamp = cast(datetime, time)
    except ValueError:
        return ""
    return timestamp.astimezone(ZoneInfo("UTC")).isoformat(timespec="seconds")


def substring_in_dict(dict_to_search: dict, substring: str) -> bool:
    if isinstance(dict_to_search, dict):
        return any(substring_in_dict(value, substring) for value in dict_to_search.values())
    elif isinstance(dict_to_search, list):
        return any(substring_in_dict(item, substring) for item in dict_to_search)
    else:
        return substring in str(dict_to_search)
