#!/usr/bin/env python3

from __future__ import annotations

import base64

import os
import logging
from pathlib import Path
from typing import Optional

import boto3
import botocore.session
import kubernetes as k8s

URL_TIMEOUT = 60
TOKEN_PREFIX = "k8s-aws-v1."
CLUSTER_NAME_HEADER = "x-k8s-aws-id"
BOTO_CONFIG = botocore.config.Config(retries={"max_attempts": 10}, region_name=os.environ["AWS_REGION"])

eks = boto3.client("eks", config=BOTO_CONFIG)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def build_k8s_config(cluster_name: str) -> object:
    try:
        eks_cluster_data = eks.describe_cluster(name=cluster_name)["cluster"]
    except eks.exceptions.ResourceNotFoundException as e:
        logger.error(e.response["Error"]["Message"])
        return None

    eks_cluster_api_role_arn = (
        os.environ["k8s_cluster_api_role"]
        if "k8s_cluster_api_role" in os.environ and len(os.environ["k8s_cluster_api_role"]) > 0
        else None
    )

    # We have to write the certificate to disk as the kubnernetes module will pass the
    # path string directly to urllib3 which does not support file-like objects for
    # certificates (see https://github.com/urllib3/urllib3/issues/1768).
    # Note that Lambda exeuction environment may persist items in /tmp, so we can save a
    # miniscule amount of time by checking to see if a previous execution wrote the cert
    # (as this should hardly-if-ever change).
    ca_cert_file = Path("/tmp/ca.crt")
    if not ca_cert_file.is_file():
        with open(ca_cert_file, "wb") as f:
            f.write(base64.b64decode(eks_cluster_data["certificateAuthority"]["data"]))

    k8s_conf = k8s.client.Configuration()
    k8s_conf.ssl_ca_cert = str(ca_cert_file)
    k8s_conf.host = eks_cluster_data["endpoint"]
    k8s_conf.api_key_prefix["authorization"] = "Bearer"
    k8s_conf.api_key["authorization"] = _get_token(
        cluster_name=eks_cluster_data["name"], role_arn=eks_cluster_api_role_arn
    )
    return k8s_conf


# Currently, awscli's `eks get-token` command is a customization present only in the CLI tool
# itself and has no equivalent in boto3/botocore. Thus, everthing below this point is lifted
# from awscli's EKS "get-token" customization as suggested by boto contributors on this
# issue: https://github.com/boto/boto3/issues/2309


# https://github.com/aws/aws-cli/blob/e1dc9ce3f422dd71fa5d971ec9bab6dfc579a825/awscli/customizations/eks/get_token.py#L63
# Lifted mostly from `_run_main` method; excludes code for passing role_arn to
# STSClientFactory.get_sts_client (though we may yet need that) and building the ExecCredential
# object with an expiration timestamp as we're only interested in the token itself and Lambda
# functions have a maximum execution time of 15m anyways (default STS token expiration)
def _get_token(cluster_name: str, role_arn: Optional[str]) -> str:
    session = botocore.session.get_session()
    client_factory = STSClientFactory(session)
    sts_client = client_factory.get_sts_client(region_name=os.environ["AWS_REGION"], role_arn=role_arn)
    token = TokenGenerator(sts_client).get_token(cluster_name)
    return token


# Disabling black formatting for easier diffing/updating of these classes/methods
# fmt: off
# https://github.com/aws/aws-cli/blob/e1dc9ce3f422dd71fa5d971ec9bab6dfc579a825/awscli/customizations/eks/get_token.py#L90
class TokenGenerator(object):
    def __init__(self, sts_client):
        self._sts_client = sts_client

    def get_token(self, cluster_name):
        """ Generate a presigned url token to pass to kubectl. """
        url = self._get_presigned_url(cluster_name)
        token = TOKEN_PREFIX + base64.urlsafe_b64encode(
            url.encode('utf-8')).decode('utf-8').rstrip('=')
        return token

    def _get_presigned_url(self, cluster_name):
        return self._sts_client.generate_presigned_url(
            'get_caller_identity',
            Params={'ClusterName': cluster_name},
            ExpiresIn=URL_TIMEOUT,
            HttpMethod='GET',
        )


# https://github.com/aws/aws-cli/blob/e1dc9ce3f422dd71fa5d971ec9bab6dfc579a825/awscli/customizations/eks/get_token.py#L110
class STSClientFactory(object):
    def __init__(self, session):
        self._session = session

    def get_sts_client(self, region_name=None, role_arn=None):
        client_kwargs = {
            'region_name': region_name
        }
        if role_arn is not None:
            creds = self._get_role_credentials(region_name, role_arn)
            client_kwargs['aws_access_key_id'] = creds['AccessKeyId']
            client_kwargs['aws_secret_access_key'] = creds['SecretAccessKey']
            client_kwargs['aws_session_token'] = creds['SessionToken']
        sts = self._session.create_client('sts', **client_kwargs)
        self._register_cluster_name_handlers(sts)
        return sts

    def _get_role_credentials(self, region_name, role_arn):
        sts = self._session.create_client('sts', region_name)
        return sts.assume_role(
            RoleArn=role_arn,
            RoleSessionName='EKSGetTokenAuth'
        )['Credentials']

    def _register_cluster_name_handlers(self, sts_client):
        sts_client.meta.events.register(
            'provide-client-params.sts.GetCallerIdentity',
            self._retrieve_cluster_name
        )
        sts_client.meta.events.register(
            'before-sign.sts.GetCallerIdentity',
            self._inject_cluster_name_header
        )

    def _retrieve_cluster_name(self, params, context, **kwargs):
        if 'ClusterName' in params:
            context['eks_cluster'] = params.pop('ClusterName')

    def _inject_cluster_name_header(self, request, **kwargs):
        if 'eks_cluster' in request.context:
            request.headers[
                CLUSTER_NAME_HEADER] = request.context['eks_cluster']


# fmt: on
