#!/usr/bin/env python3

import json
import os
import re
import sys
import time
import traceback
import requests

from dateutil import parser


AWS_ACCOUNT_ID = os.environ["AWS_ACCOUNT_ID"]
AWS_REGION = os.environ["AWS_REGION"]
AWS_LAMBDA_URL_BASE = f"https://{AWS_REGION}.console.aws.amazon.com/lambda/home?region={AWS_REGION}"
AWS_LOGS_URL_BASE = f"https://{AWS_REGION}.console.aws.amazon.com/cloudwatch/home?region={AWS_REGION}"
AWS_SFN_URL_BASE = f"https://{AWS_REGION}.console.aws.amazon.com/states/home?region={AWS_REGION}"


def generate_pretty_duration(start_time, end_time):
    """Given two unix timestamps, produces a duration broken out by hours, minutes, and seconds; drops leading zeros and units equal to zero"""
    duration_values = time.strftime("%-H|%-M|%-S", time.gmtime(end_time - start_time)).split("|")
    duration_with_units = []
    for i, unit in enumerate(["h", "m", "s"]):
        if duration_values[i] != "0":
            duration_with_units.append(f"{duration_values[i]}{unit}")
    return " ".join(duration_with_units)


def send_generic_webhook(message_type, message_text, webhook_url, event, sfn_context):
    """Sends a basic webhook with "message" and "text" params (compatible with many services)"""
    message = f"[{message_type.upper()}] - {AWS_REGION}: {message_text}"
    if message_type == "failure":
        try:
            message = message + f' ({event["error_details"]["error"]})'
        except KeyError:
            pass
    if sfn_context:
        message = message + f" (view run: {AWS_SFN_URL_BASE}#/executions/details/{sfn_context['Execution']['Id']})"
    webhook_data = {"message": message, "text": message}
    print(json.dumps(webhook_data))
    response = requests.post(
        webhook_url,
        data=json.dumps(webhook_data),
        headers={"Content-Type": "application/json"},
    )
    return response


def send_slack_webhook(message_type, message_text, webhook_url, event, sfn_context):  # noqa: C901
    """Sends a Slack-specific webhook with nice formatting for extra information"""

    # look for the stack name in the event message; make it bold by wrapping with asterisks
    if "service" in event and "name" in event["service"]:
        message_text = re.sub(rf'(.*)\b({event["service"]["name"]})\b(.*)', r"\1*\2*\3", message_text)

    # stub out the message
    message = f":backmac_{message_type}:  {message_text}"

    # if this is a failure, try to add the error type to the message
    if message_type == "failure":
        try:
            message = message + f' ({event["error_details"]["error"]})'
        except KeyError:
            pass

    # stub out the webhook data structure
    webhook_data = {"blocks": [{"type": "section", "text": {"type": "mrkdwn", "text": message}}]}

    # add various links and actions (if we have them) to an overflow_menu
    overflow_menu = []
    # if 'backup_machine' in event and 'run_id' in event and 'end_tstamp' in event:
    if sfn_context and sfn_context["State"]["Name"] in {"FailNotification", "SuccessNotification"}:
        overflow_menu.append(
            {
                "text": {
                    "type": "plain_text",
                    "text": ":aws_stepfunctions:  View Run",
                    "emoji": True,
                },
                "url": f"{AWS_SFN_URL_BASE}#/executions/details/{sfn_context['Execution']['Id']}",
            }
        )
    if message_type == "failure":
        try:
            overflow_menu.append(
                {
                    "text": {
                        "type": "plain_text",
                        "text": ":aws_cloudwatch:  View Logs",
                        "emoji": True,
                    },
                    "url": f'{AWS_LOGS_URL_BASE}#logEventViewer:group={event["error_details"]["log_group_name"]};stream={event["error_details"]["log_stream_name"]}',
                }
            )
            overflow_menu.append(
                {
                    "text": {
                        "type": "plain_text",
                        "text": ":aws_lambda:  View Lambda",
                        "emoji": True,
                    },
                    "url": f'{AWS_LAMBDA_URL_BASE}#/functions/{event["error_details"]["function_name"]}?tab=graph',
                }
            )
        except KeyError:
            pass

    # add assembled overflow items to message
    if len(overflow_menu) > 1:
        webhook_data["blocks"][0]["accessory"] = {
            "type": "overflow",
            "options": overflow_menu,
        }
    elif len(overflow_menu) == 1:
        # overflow menus must have more than 1 item; convert to button if we only have 1
        overflow_menu[0]["type"] = "button"
        webhook_data["blocks"][0]["accessory"] = overflow_menu[0]

    # look for error details we can add to the message
    if message_type == "failure":
        try:
            error_message = event["error_details"]["cause"]["errorMessage"]
        except TypeError:
            error_message = event["error_details"]["cause"]
        except KeyError:
            error_message = "Unknown error; check SFN logs"

        webhook_data["blocks"].append(
            {
                "type": "section",
                "text": {"type": "mrkdwn", "text": f"*Error Details*\n{error_message}"},
            }
        )

        try:
            raw_stacktrace = event["error_details"]["cause"]["stackTrace"]
            stacktrace_lines = []
            if isinstance(raw_stacktrace[0], list):
                # in this case, raw_stacktrace will be a list of lists like: ['/var/runtime/boto3/resources/factory.py', 339, 'property_loader', 'self.load()']
                # i.e.:
                #  0 - filename
                #  1 - line number
                #  2 - function name
                #  3 - line content
                for line in raw_stacktrace:
                    stacktrace_lines.append(f'  File "{line[0]}", line {line[1]}, in {line[2]}\n    {line[3]}\n')
            else:
                # in this case, we likely just have a list of strings that are already formatted (indented with line endings, etc)
                stacktrace_lines = raw_stacktrace

            webhook_data["blocks"].append(
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f"*Stack Trace*\n```\n{''.join(stacktrace_lines)}```",
                    },
                }
            )
        except Exception:
            pass

    # add various facts about the run (if we have them) as context elements
    context_elements = []
    if "run_type" in event:
        run_type_str_format = str.upper if event["run_type"] == "api" else str.capitalize
        context_elements.append({"type": "mrkdwn", "text": f'Type: *{run_type_str_format(event["run_type"])}*'})
    context_elements.append({"type": "mrkdwn", "text": f"Region: *{AWS_REGION}*"})
    if "dr_region" in event:
        context_elements.append({"type": "mrkdwn", "text": f'DR Region: *{event["dr_region"]}*'})
    if sfn_context:
        if sfn_context["StateMachine"]["Name"] != "backup_machine":
            context_elements.append(
                {
                    "type": "mrkdwn",
                    "text": f"Machine: *<{AWS_SFN_URL_BASE}#/statemachines/view/{sfn_context['StateMachine']['Id']}|{sfn_context['StateMachine']['Name']}>*",
                }
            )
        if sfn_context["State"]["Name"] in {"FailNotification", "SuccessNotification"}:
            start_time_in_sec = parser.isoparse(sfn_context["Execution"]["StartTime"]).timestamp()
            context_elements.append(
                {
                    "type": "mrkdwn",
                    "text": f"Duration: *{generate_pretty_duration(start_time_in_sec, time.time())}*",
                }
            )
    if "ec2_class" in event:
        context_elements.append({"type": "mrkdwn", "text": f'EC2 class: *{event["ec2_class"]}*'})

    # add assembled context elements to message
    if context_elements:
        webhook_data["blocks"].append({"type": "context", "elements": context_elements})

    print(json.dumps(webhook_data))
    response = requests.post(
        webhook_url,
        data=json.dumps(webhook_data),
        headers={"Content-Type": "application/json"},
    )
    return response


def send(message_type, message_text, event, sfn_context=None):
    if event.get("webhook_notification_url"):
        webhook_notification_url = event.get("webhook_notification_url")
    elif os.environ.get("webhook_notification_url"):
        webhook_notification_url = os.environ.get("webhook_notification_url")
    else:
        print("No webhook_notification_url defined, notifications will not be sent")
        return

    print("sending webhook")
    try:
        if "hooks.slack.com" in webhook_notification_url:
            response = send_slack_webhook(message_type, message_text, webhook_notification_url, event, sfn_context)
        else:
            response = send_generic_webhook(message_type, message_text, webhook_notification_url, event)
        response.raise_for_status()
        result = " ".join([str(response.status_code), response.text])
    except Exception:
        print("webhook sending failed (not fatal); returning traceback and continuing")
        result = "".join(traceback.format_exception(*sys.exc_info()))

    return result
