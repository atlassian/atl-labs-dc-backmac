#!/usr/bin/env python3

from __future__ import annotations

from datetime import datetime
from typing import Literal, TypedDict, TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ec2.literals import SnapshotStateType as EbsSnapshotStateType

# mypy_boto3_rds.literals has no definition for SnapshotStateType
RdsSnapshotStateType = Literal["creating", "deleting", "available"]


class ServiceSnapshotTypeDef(TypedDict, total=False):
    id: str
    region: str
    type: Literal["ebs", "rds"]
    status: EbsSnapshotStateType | RdsSnapshotStateType | Literal["waiting"]
    progress: str
    snapshot_time: datetime
    created_time: datetime
    expiry_time: datetime
    db_engine: str
    db_engine_version: str


class BackmacServiceTypeDef(TypedDict, total=False):
    name: str
    clone: bool
    cluster: str
    ebs_id: str
    efs_id: str
    iac: Literal["cfn", "k8s"]
    labels: dict[str, str]
    namespace: str
    rds_id: str
